/*
 * Realtek Semiconductor Corp.
 *
 * bsp/setup.c
 *     bsp interrult initialization and handler code
 *
 * Copyright (C) 2006-2012 Tony Wu (tonywu@realtek.com)
 */
#include <linux/console.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/sched.h>

#include <asm/addrspace.h>
#include <asm/irq.h>
#include <asm/io.h>

#include <asm/bootinfo.h>
#include <asm/time.h>
#include <asm/reboot.h>

#include <asm/smp-ops.h>

#ifdef CONFIG_CPU_HAS_MIPSMT
#include <asm/mipsmtregs.h>
#endif

#include "bspchip.h"
#include "drv/gpio/gpio.h"
#include "soc/common/gpio_reg.h"


void (* hook_restart_func)(void) = NULL;

void rtk_hook_restart_function(void (*func)(void))
{
	hook_restart_func = func;
	return;
}

static uint32 regGpioControl[] = 
{
    PABCDCNR,/* Port A */
    PABCDCNR,/* Port B */
    PABCDCNR,/* Port C */
    PABCDCNR,/* Port D */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
    PEFGCNR, /* Port E */
    PEFGCNR, /* Port F */
    PEFGCNR, /* Port G */
#endif
};

static uint32 bitStartGpioControl[] =
{
    24,      /* Port A */
    16,      /* Port B */
    8,       /* Port C */
    0,       /* Port D */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
    24,      /* Port E */
    16,      /* Port F */
    8,       /* Port G */
#endif
};

static uint32 regGpioDirection[] =
{
    PABCDDIR,/* Port A */
    PABCDDIR,/* Port B */
    PABCDDIR,/* Port C */
    PABCDDIR,/* Port D */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
    PEFGDIR, /* Port E */
    PEFGDIR, /* Port F */
    PEFGDIR, /* Port G */
#endif
};

static uint32 bitStartGpioDirection[] =
{
    24,      /* Port A */
    16,      /* Port B */
    8,       /* Port C */
    0,       /* Port D */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
    24,      /* Port E */
    16,      /* Port F */
    8,       /* Port G */
#endif
};

static uint32 regGpioData[] =
{
    PABCDDAT,/* Port A */
    PABCDDAT,/* Port B */
    PABCDDAT,/* Port C */
    PABCDDAT,/* Port D */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
    PEFGDAT, /* Port E */
    PEFGDAT, /* Port F */
    PEFGDAT, /* Port G */
#endif
};

static uint32 bitStartGpioData[] =
{
    24,      /* Port A */
    16,      /* Port B */
    8,       /* Port C */
    0,       /* Port D */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
    24,      /* Port E */
    16,      /* Port F */
    8,       /* Port G */
#endif
};

static uint32 regGpioInterruptEnable[] =
{
    PABIMR,  /* Port A */
    PABIMR,  /* Port B */
    PCDIMR,  /* Port C */
    PCDIMR,  /* Port D */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
    PEFIMR,  /* Port E */
    PEFIMR,  /* Port F */
    PGIMR,   /* Port G */
#endif
};

static uint32 bitStartGpioInterruptEnable[] =
{
    16,      /* Port A */
    0,       /* Port B */
    16,      /* Port C */
    0,       /* Port D */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
    16,      /* Port E */
    0,       /* Port F */
    16,      /* Port G */
#endif
};

static uint32 PABCDDAT_shadow = 0; /* Shadow of GPIO ABCD data */
static uint32 PABCDDAT_mask = 0;   /* Shadow of GPIO ABCD mask */
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
static uint32 PEFGDAT_shadow = 0;  /* Shadow of GPIO EFG data */
static uint32 PEFGDAT_mask = 0;    /* Shadow of GPIO EFG mask */
#endif

static int32 
_setGpioSimp(drv_gpio_func_t func, drv_gpio_port_t port, uint32 pin, uint32 data)
{
    uint32  gpio_data = 0;
	
    switch (func)
    {
        case GPIO_FUNC_CONTROL:
            /* Set GPIO control */
            if (data)
            {
                REG32(regGpioControl[port]) |= (uint32)1 << (pin + bitStartGpioControl[port]);
            }
            else
            {
                REG32(regGpioControl[port]) &= ~((uint32)1 << (pin + bitStartGpioControl[port]));
            }
            break;
            
        case GPIO_FUNC_DIRECTION:
            /* Set GPIO direction */
            if (data)
            {
                REG32(regGpioDirection[port]) |= (uint32)1 << (pin + bitStartGpioDirection[port]);
            }
            else
            {
                REG32(regGpioDirection[port]) &= ~((uint32)1 << (pin + bitStartGpioDirection[port]));
            }
            break;

        case GPIO_FUNC_DATA:
            gpio_data = REG32(regGpioData[port]);
            /* If it is GPO pin, update the shadow data first */
            if (port <= GPIO_PORT_D)
            {
                if ( PABCDDAT_mask & ((uint32)1 << (pin + bitStartGpioData[port])))
                {
                    PABCDDAT_shadow &= ~((uint32)1 << (pin + bitStartGpioData[port]));
                    PABCDDAT_shadow |= ((uint32)data << (pin + bitStartGpioData[port]));
                }
                gpio_data &= ~(PABCDDAT_mask);
                gpio_data |= (PABCDDAT_shadow & PABCDDAT_mask);
            }
            else
            {
#if defined(CONFIG_SDK_RTL8389) || defined(CONFIG_SDK_RTL8328)
                if ( PEFGDAT_mask & ((uint32)1 << (pin + bitStartGpioData[port])))
                {
                    PEFGDAT_shadow &= ~((uint32)1 << (pin + bitStartGpioData[port]));
                    PEFGDAT_shadow |= ((uint32)data << (pin + bitStartGpioData[port]));
                }
                gpio_data &= ~(PEFGDAT_mask);
                gpio_data |= (PEFGDAT_shadow & PEFGDAT_mask);
#endif
            }

            /* Set GPIO port[pin] bit */
            if (data)
            {
                gpio_data |= (uint32)1 << (pin + bitStartGpioData[port]);
            }
            else
            {
                gpio_data &= ~((uint32)1 << (pin + bitStartGpioData[port]));
            }
            REG32(regGpioData[port]) = gpio_data;
            break;
            
        case GPIO_FUNC_INTERRUPT_ENABLE:
            /* Set GPIO interrupt enable bit */
            REG32(regGpioInterruptEnable[port]) &= ~((uint32)0x3 <<                             \
                    (pin*2 + bitStartGpioInterruptEnable[port]));
            REG32(regGpioInterruptEnable[port]) |= (uint32)data <<                              \
                    (pin*2 + bitStartGpioInterruptEnable[port]);
            break;
        default:    
            break;
    }

    return RT_ERR_OK;
} /* end of _setGpio */

static void bsp_machine_restart(char *command)
{
#if defined(CONFIG_RTL8380_SERIES)
    unsigned int tmp = 0;
#endif
	
	if(hook_restart_func != NULL)
	{
		hook_restart_func();
	}

#if defined(CONFIG_RTL8390_SERIES)
	printk("System restart.\n");
	REG32(0xBB000014) = 0xFFFFFFFF;    /* Reset whole chip */
#endif
#if defined(CONFIG_RTL8380_SERIES)
	OTTO838x_FLASH_DISABLE_4BYTE_ADDR_MODE();
	printk("System restart.\n");

	{
		uint32 gpioPin = 0;
		drv_gpio_port_t gpioPort = GPIO_PORT_A;
		uint32 data = 0;
		
#if defined(T1500G_8T)
		gpioPin = 1;
#elif defined(T1500_28TC)
		gpioPin = 2;
#elif defined(T1500G_10PS) || defined(T1500G_10MPS) || defined(T2600G_18TS) || defined(T1600G_18TS)
		gpioPin = 3;
#elif defined(T2500G_10MPS) || defined(T2500G_10TS)
		gpioPin = 5;
		gpioPort = GPIO_PORT_B;
#elif defined(T1500_28PCT)
		gpioPin = 4;
#elif defined(T1600G_28TS_V3)
		gpioPin = 3;
#endif

		_setGpioSimp(GPIO_FUNC_CONTROL, gpioPort, gpioPin, GPIO_CTRLFUNC_NORMAL); 
		_setGpioSimp(GPIO_FUNC_DIRECTION, gpioPort, gpioPin, GPIO_DIR_OUT);
		_setGpioSimp(GPIO_FUNC_INTERRUPT_ENABLE, gpioPort, gpioPin, GPIO_INT_DISABLE);

		_setGpioSimp(GPIO_FUNC_DATA, gpioPort, gpioPin, data);
	}
	
    //REG32(0xBB000040) = 0x1;    /* Reset Global Control1 Register */
#endif
#if defined(CONFIG_RTL8328_SERIES)
	printk("System restart.\n");
    REG32(0xBB020004) = 0x0;    /* Reset Global Control1 Register */
	while(1);
#endif
}

static void bsp_machine_halt(void)
{
#if defined(CONFIG_RTL8390_SERIES)
	printk("System halted.\n");
	while(1);
#endif
#if defined(CONFIG_RTL8380_SERIES)
	printk("System halted.\n");
	while(1);
#endif
#if defined(CONFIG_RTL8328_SERIES)
	printk("System halted.\n");
	while(1);
#endif

}

#ifdef CONFIG_MIPS_MT_SMTC
extern struct plat_smp_ops bsp_smtc_smp_ops;
#endif

extern int bsp_serial_init(void);

/* callback function */
void __init bsp_setup(void)
{

	_machine_restart = bsp_machine_restart;
    _machine_halt = bsp_machine_halt;

	bsp_serial_init();
}

EXPORT_SYMBOL(rtk_hook_restart_function);

