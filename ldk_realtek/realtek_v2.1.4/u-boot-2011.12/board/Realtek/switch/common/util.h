/*
 * Copyright(c) Realtek Semiconductor Corporation, 2008
 * All rights reserved.
 *
 * Purpose : Related definition of switch util for U-Boot.
 *
 * Feature : switch util function
 *
 */

#ifndef __UTIL_H__
#define __UTIL_H__

/*
 * Include Files
 */
#include <config.h>

/*
 * Symbol Definition
 */
#define MEDIATYPE_COPPER        (0)
#define MEDIATYPE_FIBER         (1)
#define MEDIATYPE_COPPER_AUTO   (2)
#define MEDIATYPE_FIBER_AUTO    (3)

/*
 * Data Declaration
 */

/*
 * Macro Definition
 */

/*
 * Function Declaration
 */

/* Function Name:
 *      rtk_portIdxFromMacId
 * Description:
 *      Get PHY index from MAC port id
 * Input:
 *      macId   - MAC port id
 * Output:
 *      portIdx - port index relates MAC port id
 * Return:
 *      None
 * Note:
 *      None
 */
int rtk_portIdxFromMacId(int macId, int *portIdx);

void rtk_phyPatchBit_set(int port, int page, int reg, unsigned char endBit,
    unsigned char startBit, unsigned int inVal);

#if (defined(CONFIG_RTL8214FC) || defined(CONFIG_RTL8218FB))
extern void rtl8214fc_media_set(int portid, int media);
extern void rtl8214fc_media_get(int portid, int *media);
extern void rtl8214fc_fiber_watchdog(int port);
#endif

#endif  /* __UTIL_H__ */
