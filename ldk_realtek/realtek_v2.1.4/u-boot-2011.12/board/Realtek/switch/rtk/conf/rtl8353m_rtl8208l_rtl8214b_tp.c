/*
 * Copyright(c) Realtek Semiconductor Corporation, 2010
 * All rights reserved.
 *
 * Purpose : Related implementation of the RTL8353M DEMO board for U-Boot.
 *
 * Feature : RTL8353M DEMO board database
 *
 */


/*
 * Include Files
 */


/*
 * Symbol Definition
 */


/*
 * Data Declaration
 */
static const rtk_switch_model_t rtl8353m_tp = {
    .name = "RTL8353M_TP",
    .chip = RTK_CHIP_RTL8353M,

	.led = {
	    /* LED layout settting */
	    .led_if_sel = LED_IF_SEL_SERIAL,
	    .num = 2,
	    .copr_pmask = {
		    [0] = 0xFFFFFFFF,
		    [1] = 0x000CFFFF,
	    },
	    .fib_pmask = {
		    [0] = 0x00000000,
		    [1] = 0x000F0000,
		},
		.led_combo = {
		    [0] = 0x00000000,
		    [1] = 0x00000000,
		},
	    .led_definition_set = 
		{
			[0] =
				{
					.led = 
						{
							[0] = 0xA,/* 10/100Mbps link/act */
							[1] = 0xF, /* 1000Mbps link/act */	
							[2] = 0x6,/* duplex mode */
						},
				},
			[1] =
				{
					.led = 
						{
							[0] = 0xB,/* 1000Mbps link/act */
							[1] = 0xC,/* 10/100Mbps link/act */	
							[2] = 0x6,/* duplex mode */
						},
				},
			[2] =
				{
					.led = 
						{
							[0] = 0x8,/* 1000Mbps link/act */
							[1] = 0x7,/* 10/100Mbps link/act */	
							[2] = 0x0,/* duplex mode */
						},
				},
			[3] =
				{
					.led = 
						{
							[0] = 0xA,/* 1000Mbps link/act */
							[1] = 0xB,/* 10/100Mbps link/act */	
							[2] = 0xB,/* duplex mode */
						},
				},
	    },
	    /* LED definition selection (per-port) */
	    .led_copr_set_psel_bit0_pmask =
		{
			[0] = 0xFFFFFFFF,
			[1] = 0x0000FFFF,
		},
		.led_copr_set_psel_bit1_pmask =
		{
			[0] = 0x00000000,
			[1] = 0x00000000,
		},
		.led_fib_set_psel_bit0_pmask =
		{
			[0] = 0x00000000,
			[1] = 0x00030000,
		},
		.led_fib_set_psel_bit1_pmask =
		{
			[0] = 0x00000000,
			[1] = 0x00030000,
		},
    },

	.port = {
	    .count = 52,
	    .list = {
	        { .mac_id = 0,  .phy_idx = 0, .phy = 0 },
	        { .mac_id = 1,  .phy_idx = 0, .phy = 1 },
	        { .mac_id = 2,  .phy_idx = 0, .phy = 2 },
	        { .mac_id = 3,  .phy_idx = 0, .phy = 3 },
	        { .mac_id = 4,  .phy_idx = 0, .phy = 4 },
	        { .mac_id = 5,  .phy_idx = 0, .phy = 5 },
	        { .mac_id = 6,  .phy_idx = 0, .phy = 6 },
	        { .mac_id = 7,  .phy_idx = 0, .phy = 7 },
	        { .mac_id = 8,  .phy_idx = 1, .phy = 0 },
	        { .mac_id = 9,  .phy_idx = 1, .phy = 1 },
	        { .mac_id = 10, .phy_idx = 1, .phy = 2 },
	        { .mac_id = 11, .phy_idx = 1, .phy = 3 },
	        { .mac_id = 12, .phy_idx = 1, .phy = 4 },
	        { .mac_id = 13, .phy_idx = 1, .phy = 5 },
	        { .mac_id = 14, .phy_idx = 1, .phy = 6 },
	        { .mac_id = 15, .phy_idx = 1, .phy = 7 },
	        { .mac_id = 16, .phy_idx = 2, .phy = 0 },
	        { .mac_id = 17, .phy_idx = 2, .phy = 1 },
	        { .mac_id = 18, .phy_idx = 2, .phy = 2 },
	        { .mac_id = 19, .phy_idx = 2, .phy = 3 },
	        { .mac_id = 20, .phy_idx = 2, .phy = 4 },
	        { .mac_id = 21, .phy_idx = 2, .phy = 5 },
	        { .mac_id = 22, .phy_idx = 2, .phy = 6 },
	        { .mac_id = 23, .phy_idx = 2, .phy = 7 },
	        { .mac_id = 24, .phy_idx = 3, .phy = 0 },
	        { .mac_id = 25, .phy_idx = 3, .phy = 1 },
	        { .mac_id = 26, .phy_idx = 3, .phy = 2 },
	        { .mac_id = 27, .phy_idx = 3, .phy = 3 },
	        { .mac_id = 28, .phy_idx = 3, .phy = 4 },
	        { .mac_id = 29, .phy_idx = 3, .phy = 5 },
	        { .mac_id = 30, .phy_idx = 3, .phy = 6 },
	        { .mac_id = 31, .phy_idx = 3, .phy = 7 },
	        { .mac_id = 32, .phy_idx = 4, .phy = 0 },
	        { .mac_id = 33, .phy_idx = 4, .phy = 1 },
	        { .mac_id = 34, .phy_idx = 4, .phy = 2 },
	        { .mac_id = 35, .phy_idx = 4, .phy = 3 },
	        { .mac_id = 36, .phy_idx = 4, .phy = 4 },
	        { .mac_id = 37, .phy_idx = 4, .phy = 5 },
	        { .mac_id = 38, .phy_idx = 4, .phy = 6 },
	        { .mac_id = 39, .phy_idx = 4, .phy = 7 },
	        { .mac_id = 40, .phy_idx = 5, .phy = 0 },
	        { .mac_id = 41, .phy_idx = 5, .phy = 1 },
	        { .mac_id = 42, .phy_idx = 5, .phy = 2 },
	        { .mac_id = 43, .phy_idx = 5, .phy = 3 },
	        { .mac_id = 44, .phy_idx = 5, .phy = 4 },
	        { .mac_id = 45, .phy_idx = 5, .phy = 5 },
	        { .mac_id = 46, .phy_idx = 5, .phy = 6 },
	        { .mac_id = 47, .phy_idx = 5, .phy = 7 },
	        { .mac_id = 48, .phy_idx = 6, .phy = 0 },
	        { .mac_id = 49, .phy_idx = 6, .phy = 1 },
	        { .mac_id = 50, .phy_idx = 6, .phy = 2 },
	        { .mac_id = 51, .phy_idx = 6, .phy = 3 },
	    },  /* port.list */
    },

    .serdes = {
	    .count = 13,
	    .list = {
	        /* mii: SR4_CFG_FRC_SDS_MODE */
	        { .sds_id = 0,  .phy_idx = 0, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 1,  .phy_idx = 0, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 2,  .phy_idx = 1, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 3,  .phy_idx = 1, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 4,  .phy_idx = 2, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 5,  .phy_idx = 2, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 6,  .phy_idx = 3, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 7,  .phy_idx = 3, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 8,  .phy_idx = 4, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 9,  .phy_idx = 4, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 10, .phy_idx = 5, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 11, .phy_idx = 5, .mii = RTK_MII_RSGMII_PLUS },
	        { .sds_id = 12, .phy_idx = 6, .mii = RTK_MII_RSGMII_PLUS },
	    },  /* serdes.list */
    },

	.phy = {
	    .baseid = 0,
	    .count = 7,
	    .list = {
	        [0] = { .chip = RTK_CHIP_RTL8208L,  .mac_id = 0 , .phy_max = 8},
	        [1] = { .chip = RTK_CHIP_RTL8208L,  .mac_id = 8 , .phy_max = 8},
	        [2] = { .chip = RTK_CHIP_RTL8208L,  .mac_id = 16, .phy_max = 8},
	        [3] = { .chip = RTK_CHIP_RTL8208L,  .mac_id = 24, .phy_max = 8},
	        [4] = { .chip = RTK_CHIP_RTL8208L,  .mac_id = 32, .phy_max = 8},
	        [5] = { .chip = RTK_CHIP_RTL8208L,  .mac_id = 40, .phy_max = 8},
	        [6] = { .chip = RTK_CHIP_RTL8214B,  .mac_id = 48, .phy_max = 4},
	    },   /* .phy.list */
    },
};

