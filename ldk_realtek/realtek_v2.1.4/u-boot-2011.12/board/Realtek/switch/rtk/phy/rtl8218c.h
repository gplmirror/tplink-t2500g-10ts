/*
 * Copyright(c) Realtek Semiconductor Corporation, 2014
 * All rights reserved.
 *
 * Purpose : Related implementation of the RTL8218 PHY driver.
 *
 * Feature : RTL8218 PHY driver
 *
 */

#ifndef	__RTL8218C_H__
#define	__RTL8218C_H__

/*
 * Include Files
 */

/*
 * Symbol Definition
 */

/*
 * Data Declaration
 */

/*
 * Macro Definition
 */

/*
 * Function Declaration
 */
extern void rtl8218c_config(Tuint8 macId);

#endif	/* __RTL8218C_H__ */

