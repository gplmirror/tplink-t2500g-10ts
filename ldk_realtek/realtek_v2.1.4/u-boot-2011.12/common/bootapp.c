/*! Copyright(c) 2008-2010 Shenzhen TP-LINK Technologies Co.Ltd.
 *
 *\file		bootapp.c
 *\brief		
 *\details	
 *
 *\author	Cai Peifeng
 *\version	
 *\date		02Sep10
 *
 *\warning	
 *
 *\history \arg	02Sep10, Cai Peifeng, Create the file
 */
/**********************************************************************/
/*                          CONFIGURATIONS                            */
/**********************************************************************/
/**********************************************************************/
/*                          INCLUDE_FILES                             */
/**********************************************************************/

/*from uboot*/
#include<common.h>
#include<linux/ctype.h>
#include<vsprintf.h>
#include<linux/time.h>
#include<rtc.h>
#include<jffs2/jffs2.h>

/*our files*/
#include<tplink/flashDrvLib.h>
#include<tplink/flashAppFsImage.h>
#include <tplink/flashAppInner.h>
#include <tplink/flashConfig.h>
#include <tplink/flashAppInnerProfile.h>
#include<tplink/flashUtil.h>



/**********************************************************************/
/*                           DEFINES                                  */
/**********************************************************************/

#ifndef bool
#define bool int
#endif

#define TFTP_LOAD_ADDR		0x81000000	
//#define UBOOT_DEBUG
#define BUF_LINE			255		/*!< the max characters of a line*/
#define CMD_LEN				32

#define BOOT_ADDR_LEN		30
#define BOOT_FILE_LEN		160	/* max chars in file name */
#define BOOT_USR_LEN		20	/* max chars in user name */
#define BOOT_PASSWORD_LEN	20	/* max chars in password */		
#define MAC_ADDRESS_LENGTH	6
#define BOOT_VERSION		"1.0.0"


#define MAC_ADDRESS_LENGTH	6
#define IP_ADDRESS_LENGTH	17


#define SQUASHFS_HEADER_MAGIC_BH  0x6873
#define SQUASHFS_HEADER_MAGIC_TH   0x7173
/* 判断是否输入数字 */
#define IS_NOT_DIGITAL_LINE(line)    (_isDigitalString(line) == 0)    

#define ERR_NOT_DIGITAL_PRINT()      printf("Input string contains illegal characters.\r\n")

#define IS_NOT_NEW_LINE(line)		(strlen(line) != 0) 

/* 直接RAM启动配置选项 */
 #define TP_KERNEL_IMAGE_NAME   "uImage.img"
 #define TP_KERNEL_IMAGE_DL_ADDR 0x81000000
 #define TP_KERNEL_IMAGE_MAX_SIZE 8*1024*1024 /*8M*/


 #define TP_USER_IMAGE_NAME   "usrimage.jffs2"
 
 /*   
 	@def   : TP_USER_IMAGE_DL_ADDR
 	@brief : 将usrimage.jffs2下载到RAM的地址
	linux配置文件linux-3.6.5-flash.config(buildroot\board\broadcom\hurricane2目录下)的
	配置项CONFIG_TPLINK_ROOFS_LOAD_ADDR需要与该值对应，且为RAM的偏移地址:
	CONFIG_TPLINK_ROOFS_LOAD_ADDR = TP_USER_IMAGE_DL_ADDR - RAM的基地址(0x60000000)
 */
 #define TP_USER_IMAGE_DL_ADDR  0x83000000 

 /*
	@def   : TP_USER_IMAGE_MAX_SIZE
	@brief : usrimage.jffs2的最大大小
 */
 #define TP_USER_IMAGE_MAX_SIZE 10*1024*1024  /*10M*/


/**********************************************************************/
/*                            TYPES                                   */
/**********************************************************************/

typedef struct
{
	char ipAddr[BOOT_ADDR_LEN];			/*!< the ip of the switch*/
	char ipMask[BOOT_ADDR_LEN];			/*!< the mask of the switch*/
	char routeAddr[BOOT_ADDR_LEN];		/*!< the route of the switch*/
	char hostAddr[BOOT_ADDR_LEN];		/*!< the tftp host address*/
	char fileName[BOOT_FILE_LEN];		/*!< the file that to be download*/
	int flags;							/*!< the current mode*/
}BOOT_CONFIG;

typedef struct _cmdStruct
{
	bool	(*cmdFunc)(void);				/*命令回调函数*/
	char*	description;				/*功能描述*/
}cmdStruct;
/**********************************************************************/
/*                            FUNCTIONS                               */
/**********************************************************************/

void printBootromLogo(void);
bool cmdHelp(void);
bool cmdReboot(void);
bool cmdReset(void);
bool cmdStart(void);
bool cmdActivateBackupImage(void);
bool cmdShowImage(void);
bool cmdPwdRecovery(void);
#ifdef UBOOT_DEBUG
bool cmdIfconfig(void);
bool cmdTftp(void);
bool cmdDeleteBackupImage(void);
bool cmdDeleteCfg(void);
bool cmdUpdateUboot(void);
bool cmdUpdate(void);
bool cmdUpdateCfg(void);
bool cmdExitMenu(void);
bool cmdProfile(void);
bool cmdFlashTest(void);
bool cmdFlashReadTest(void);
void AutoStart(void);
bool cmdStartImageFromRam(void);
bool cmdUploadInner(void);
bool cmdLoadSoftWare(void);
#endif

/**********************************************************************/
/*                            VARIABLES                               */
/**********************************************************************/

/*!
  var				bootCommand
   \brief			the comand list
   \description	
   \range		
   \usage		
*/
cmdStruct bootCommand[] = 
{
    {cmdHelp,					"- Print this boot menu"},
    {cmdReboot, 				"- Reboot"},
    {cmdReset,					"- Reset"},
    {cmdStart,					"- Start"},
    {cmdActivateBackupImage,	"- Activate Backup Image"},
    {cmdShowImage,              "- Display image(s) info"},	
    {cmdPwdRecovery,            "- Password recovery"},
#ifdef UBOOT_DEBUG
    {cmdIfconfig,				"- Set ip address"},
    {cmdUpdate,					"- Download a image file and update"},
    {cmdTftp,                   "- Set Tftp parameter"},
    {cmdDeleteBackupImage,      "- Delete the Backup Image file"},
    {cmdUpdateUboot,        	"- Download u-boot.bin and update"},
    {cmdProfile,                "- Download profile and update"},
    {cmdUpdateCfg,				"- Download a configure file and update."},
    {cmdExitMenu,               "- Exit menu"},
    {cmdFlashTest,				"- Test flash driver"},
    {cmdFlashReadTest,			"- Test flash driver of read"},
	{cmdStartImageFromRam,		"- Download the image and startup from RAM"},
	{cmdUploadInner,			"- Upload the Inner for flash image"},
	{cmdLoadSoftWare,			"- Load software to flash"},
#endif	
	{NULL,NULL}
};


static char* imageName[2] = 
{
	"image1.bin",
	"image2.bin"	
};	


static int g_ExitMenuFlag = 0;


/**********************************************************************/
/*                            EXTERN_PROTOTYPES                       */
/**********************************************************************/

extern int readline_into_buffer(const char *const prompt, 
								char *buffer);


extern IPaddr_t string_to_ip(const char *s);

extern void ip_to_string(IPaddr_t x, char *s);

extern void to_tm(int tim, struct rtc_time * tm);

extern int sysGetFlAddr(char * key);
extern int AutoFixKernel(void);
extern int sysSetActiveInfo(int nRegion);
extern int sysSetActiveInfo(int nRegion);
extern ulong getenv_ulong(const char *name, int base, ulong default_val);
extern int   flashAppInit(void);
extern int sysSetActiveInfoWithOutFlash(int nRegion, int baudrate);
extern int sysGetMtdpartsInfo(char * key);
extern int sysFixKernel(char * pImageBuff, int len);

#ifdef UBOOT_DEBUG
//extern int bootWriteFile(const  char* pImageBuff, int nLength, int nRegion, IMAGE_INFO stImageInfo);
extern int sysLoadSoftWare(char * pImageBuff, int len);
extern int   flashAppInit(void);
extern UINT32 readFlash(char *source, unsigned int size, unsigned int flashOffset);
extern int sysEraseImage(int nRegion);
extern int sysUpdateCfg(const char * pImageBuff, int nLength);
extern int sysEraseCfg(void);
extern int sysUpdateUboot(const  char* pImageBuff, int nLength);

#endif

/**********************************************************************/
/*                            LOCAL_PROTOTYPES                        */
/**********************************************************************/
/**********************************************************************/
/*                            LOCAL_FUNCTIONS                         */
/**********************************************************************/

#define is_digit(c)	((c) >= '0' && (c) <= '9')
#if 0
static int skip_atoi(const char **s)
{
	int i=0;

	while (is_digit(**s))
		i = i*10 + *((*s)++) - '0';
	return i;
}
#endif


static int _isDigitalString(const char* str)
{
    int len = 0;
    char *temp = NULL;

    if ((str == NULL) ||
       ((len = strlen(str)) == 0))
    {
        return 0;
    }

    temp = (char *)str;
    while (*temp != '\0')
    {
        if (((*temp) < '0') ||
           ((*temp) > '9'))
        {
            return 0;
        }
        temp++;
    }
    return 1;
}

/******************************************************************************
*
*
* skipSpace - advance pointer past white space
*
* Increments the string pointer passed as a parameter to the next
* non-white-space character in the string.
*/
void skipSpace(char **strptr)
{
	char *pLine;
	int len;
	
	if( NULL == strptr || NULL == *strptr)
	{
		return;
	}
	
	/*skip the last space*/
	len = strlen( *strptr);

	pLine = *strptr + len - 1;

	while( pLine >= *strptr && (*pLine == ' ') )
	{
		*pLine = EOS;
		pLine --;
	}

	/*skip the first space*/
	while( isspace( (UINT32) ** strptr) )
		++ *strptr;	
}


/*!
 *\fn			bool cmdReadLine(char *param)
 *\brief   		read a line
 *
 *\param[in]   
 *\param[out]  
 *
 *\return  
 *\retval  
 *
 *\note		  
 */
static bool cmdReadLine(char *param, const char *const prompt)
{
	int len = 0;

	if( NULL == param)
	{
		return FALSE;
	}
	
	/*read string*/
	len = readline_into_buffer(prompt, param);

	if( 0 == len)
	{
		return FALSE;
	}
	else
	{
		param[len] = '\0';
	}

	/*turn the line feed to end*/
	if(param[strlen(param)-1] == 0xa)
	{
		param[strlen(param)-1] = EOS;
	}

	return TRUE;
}

#if 0
/*!
   \function	cmdGetName(char *param,char *pName)	
   \brief		
   \descrition	get the commander name of the string.
   \param[in]	param:the string that to be parsse.
   \param[out]	pName:store the commander name.
   \return		TRUE:success;FALSE:some error has occur.
   \calls		
   \others		
*/
static bool cmdGetName(char *param, char *pName)
{
	int len;
	int i;
	
	if(NULL == param || NULL == pName)
	{
		return FALSE;
	}

	len = strlen(param);
	
	if( 0 == len)
	{
		return FALSE;
	}
	
	for(i=0; i < len; i++)
	{
		if( i > CMD_LEN)
		{
			return FALSE;
		}
		if(isspace((int)param[i]) )
		{
			break;
		}
	}
	
	strncpy(pName,param,i);
	pName[i] = '\0';
	return TRUE;	
}

/*!
   \function	cmdGetNextArg(char **param,char *pName,int *pEnd)	
   \brief		get the next argument
   \descrition	
   \param[in]	param:the string that to be parse
   \param[out]	pName:store the next argument
   \param[out]  pEnd:the param has the next argument or not.
   				FALSE:has the next argument;TRUE:not has the next argument.
   \return		FALSE:some error has occur.TRUE:success
   \calls		
   \others		
*/
static int cmdGetNextArg(char **param, char *pName, int *pEnd)
{
	int len;
	int i;
	char *pFirst;

	if( NULL == param || NULL == *param || NULL == pEnd)
	{
		return FALSE;
	}
	
	skipSpace(param);

	if (**param == EOS)
	{
		*pEnd = TRUE;
		return TRUE;
	}
	
	len = strlen(*param);
	
	pFirst = *param;
	
	for(i=0; i < len; i++)
	{
		if( i > BUF_LINE)
		{
			printf("The param length must less than 255.\r\n");
			*pEnd = FALSE;
			return FALSE;
		}
		if(isspace((int)**param))
		{
			break;
		}
		(*param)++;					
	}

	if( NULL != pName)
	{
		strncpy(pName,pFirst,i);
		pName[i] = '\0';
	}

	*pEnd = FALSE;
	
	return TRUE;	
}
#endif

/*!
 *\fn			void cmdParseCmd(char *param)
 *\brief   		parse the command and run the comand
 *
 *\param[in]   
 *\param[out]  
 *
 *\return  
 *\retval  
 *
 *\note		  
 */
void cmdParseCmd(char *param)
{
	
    int index = 0;
    int cmdNumber = 0;
    bool rc = TRUE;

    if (NULL == param)
    {
        return;
    }

    if (strlen(param) == 0)
    {
        return;
    }

    if (strncmp(param, "?", BUF_LINE) == 0)
    {
        cmdHelp();
        return;
    }

    cmdNumber = sizeof(bootCommand) / sizeof(cmdStruct) - 1;

    if (IS_NOT_DIGITAL_LINE(param))
    {
        ERR_NOT_DIGITAL_PRINT();
        return;
	}

    index = simple_strtoul(param, NULL, 10);

	if (index < 0 || index >= cmdNumber)
	{
		printf("Invalid boot option.\r\n");
	}	
    if ((index >= 0) && (index < cmdNumber) && bootCommand[index].cmdFunc != NULL)
    {
        rc = bootCommand[index].cmdFunc();
    }
    return;	

}




/*!
 *\fn			bool ftpGetImage(int fSocket,char **pData,int *fileLength, int maxSize)
 *\brief   
 *
 *\param[in]   
 *\param[out]  
 *
 *\return  
 *\retval  
 *
 *\note		  
 */
static bool ftpGetImage(char **pData, char* pFileName,int *fileLength, int maxSize)
{
	char *pBuf;				
    char command[CONFIG_SYS_CBSIZE] = {0};
    int flag = 0;

    if (NULL == pFileName || NULL == fileLength)
    {
        return FALSE;
    }    
    #if 0
    sprintf(command, "rtk network on");
    run_command(command, flag);
    #endif
    pBuf = (char *)TFTP_LOAD_ADDR;
	memset(command,0,CONFIG_SYS_CBSIZE);
    sprintf(command, "tftpboot 0x%0x %s 0x%0x 0x%0x", (unsigned int)pBuf, pFileName, (unsigned int)fileLength, maxSize);
    //printf("%s:%s,0x%x\n",__FUNCTION__,command,(unsigned int)*fileLength);
    run_command(command, flag);

	*pData = pBuf;
	return TRUE;

}

static void sysReboot(void)
{
    char command[CONFIG_SYS_CBSIZE] = {0};
    int flag = 0; 
    sprintf(command, "reset");
    run_command(command, flag); 
}


#if 0
/*!
   \function	helpIfconfig()
   \brief		show the ifconfig help.
   \descrition
   \param[in]	N/A
   \param[out]	N/A
   \return
   \calls
   \others
*/
static void helpIfconfig(void)
{
	printf("Usage (ifConfig): Parameters: [[<option> <value> ...]]\r\n");
	printf("With no parameters, all current interfaces are listed.\r\n");
	printf("Value options are:\r\n");
	printf("\tip <address>		- IP address associated with interface\r\n");
	printf("\tmask <value>		- Subnet mask associated with interface\r\n");
	printf("\tgateway <adress>	- Default gateway\r\n");
}
#endif

#if 1
static image_header_t *image_get_kernel(ulong img_addr, int verify)
{
	image_header_t *hdr = (image_header_t *)img_addr;

	if (!image_check_magic(hdr)) {
		/*puts("Bad Magic Number\n");*/
		//bootstage_error(BOOTSTAGE_ID_CHECK_MAGIC);
		return NULL;
	}
	//bootstage_mark(BOOTSTAGE_ID_CHECK_HEADER);

	if (!image_check_hcrc(hdr)) {
		/*puts("Bad Header Checksum\n");*/
		//bootstage_error(BOOTSTAGE_ID_CHECK_HEADER);
		return NULL;
	}

	//bootstage_mark(BOOTSTAGE_ID_CHECK_CHECKSUM);
	/*image_print_contents(hdr);*/

	/*if (verify) {
		puts("   Verifying Checksum ... ");
		if (!image_check_dcrc(hdr)) {
			printf("Bad Data CRC\n");
			bootstage_error(BOOTSTAGE_ID_CHECK_CHECKSUM);
			return NULL;
		}
		puts("OK\n");
	}*/
	//bootstage_mark(BOOTSTAGE_ID_CHECK_ARCH);

	if (!image_check_target_arch(hdr)) {
		/*printf("Unsupported Architecture 0x%x\n", image_get_arch(hdr));*/
		//bootstage_error(BOOTSTAGE_ID_CHECK_ARCH);
		return NULL;
	}
	return hdr;
}


static image_header_t* getImageHeadInfo(ulong img_addr, ulong* os_data, ulong* os_len)
{
	
	image_header_t	*hdr = NULL;
	
	/* copy from dataflash if needed */
	img_addr = genimg_get_image(img_addr);

	
	//bootstage_mark(BOOTSTAGE_ID_CHECK_MAGIC);

	/* check image type, for FIT images get FIT kernel node */
	*os_data = *os_len = 0;
	switch (genimg_get_format((void *)img_addr)) {
		case IMAGE_FORMAT_LEGACY:
			/*printf("## Booting kernel from Legacy Image at %08lx ...\n",
					img_addr);*/
			hdr = image_get_kernel(img_addr, 0);
			if (!hdr)
				return NULL;
			//bootstage_mark(BOOTSTAGE_ID_CHECK_IMAGETYPE);

			/* get os_data and os_len */
			switch (image_get_type(hdr)) {
			case IH_TYPE_KERNEL:
			case IH_TYPE_KERNEL_NOLOAD:
				*os_data = image_get_data(hdr);
				*os_len = image_get_data_size(hdr);
				break;
			case IH_TYPE_MULTI:
				image_multi_getimg(hdr, 0, os_data, os_len);
				break;
			case IH_TYPE_STANDALONE:
				*os_data = image_get_data(hdr);
				*os_len = image_get_data_size(hdr);
				break;
			default:
				/*printf("Wrong Image Type for\n");*/
				//bootstage_error(BOOTSTAGE_ID_CHECK_IMAGETYPE);
				return NULL;
			}
		//bootstage_mark(BOOTSTAGE_ID_DECOMP_IMAGE);
		break;
	default:
		/*printf("Wrong Image Format\n");*/
		//bootstage_error(BOOTSTAGE_ID_FIT_KERNEL_INFO);
		return NULL;
	}

	return hdr;

}

#endif

static bool imageFileExist(ulong nRegion)
{
    image_header_t* phdr = NULL;
    ulong os_data = 0;
    ulong os_len = 0;
    int img_addr = 0;
    //int kernel_addr = 0;
    //int i = 0;
    struct jffs2_unknown_node head;
	struct jffs2_unknown_node crcnode;
	uint32_t crc;
    int nRet = ERROR;


    phdr = getImageHeadInfo(IMAGE_ADDR, &os_data, &os_len);

    if (NULL == phdr)
    {
        nRet = AutoFixKernel();
        if(ERROR == nRet)
        {
            return FALSE;
        }        
    }

    img_addr = (1 == nRegion)? (sysGetFlAddr("usrimg1")):(sysGetFlAddr("usrimg2"));
    memset(&head, 0, sizeof(head));

    l_readFlash((char *)&head, sizeof(struct jffs2_unknown_node), img_addr);

	/*check magic number*/
	if (head.magic != JFFS2_MAGIC_BITMASK)
	{
		//printf("wrong magic number %d\n", head.magic);
		
		
		if (SQUASHFS_HEADER_MAGIC_BH == head.magic
		&& SQUASHFS_HEADER_MAGIC_TH == head.nodetype)
		{
			return TRUE;
		}
		
		return FALSE;
	}	
	else
	{
	
	/* OK, now check for node validity and CRC */
	crcnode.magic = JFFS2_MAGIC_BITMASK;
	crcnode.nodetype = head.nodetype;
	crcnode.totlen = head.totlen;
	crc = crc32_no_comp(0, (uchar *)&crcnode, sizeof(crcnode)-4);

	if (head.hdr_crc != crc) {
		//printf("head.hdr %d, crc %d\n", head.hdr_crc, crc);
		return FALSE;
	}
	}	


    return TRUE;
    
}


int getStartUpIndex(void)
{
    char value[ENV_ITEM_VALUE_LEN] = {0};

    getEnvItem(TP_SWITCH_START_IMAGE, value);
	
    /*
    char * pArea = NULL;
    
    pArea = getenv("bootarea");

    if((pArea[0] != '1') && (pArea[0] != '2'))
    {
        return -1;
    }
	
    return pArea[0] - '1' + 1;
    */
    if(strcmp(value, TP_SWITCH_IMAGE_NAME_1) == 0)
    {
        return 1;
    }
    else if(strcmp(value, TP_SWITCH_IMAGE_NAME_2) == 0)
    {
        return 2;
    }
    else
    {
        return 1;//默认1区启动
    }
}

void setStartUpIndex(int index)
{
    char tmp[BUF_LINE] = {0};

    if (1 != index && 2 != index)
    {
    return;
    }

    if(1 == index)
    {
        setEnvItem(TP_SWITCH_START_IMAGE, TP_SWITCH_IMAGE_NAME_1);
        setEnvItem(TP_SWITCH_BACKUP_IMAGE, TP_SWITCH_IMAGE_NAME_2);
    }
    else
    {
        setEnvItem(TP_SWITCH_START_IMAGE, TP_SWITCH_IMAGE_NAME_2);
        setEnvItem(TP_SWITCH_BACKUP_IMAGE, TP_SWITCH_IMAGE_NAME_1);
    }

    sysSetActiveInfo(index);
    sprintf(tmp, "%d", index);
    setenv("bootarea", tmp);	
    saveenv();
 
}	


BOOL resaveStartupIndex(void)
{
    //char line[BUF_LINE] = {0};
    //ulong img_addr = 0;

    if(1 == getStartUpIndex())
    {
        setStartUpIndex(1);
    }
    else
    {
        setStartUpIndex(2);
    }
    

    return TRUE;
}

	
/**********************************************************************/
/*                            PUBLIC_FUNCTIONS                        */
/**********************************************************************/



//extern int rtc_get(struct rtc_time *tmp);

/*!
 *\fn			void printBootromLogo()
 *\brief   		printf bootrom logo
 *
 *\param[in]   	N/A
 *\param[out]  	N/A
 *
 *\return  		N/A
 *\retval  		N/A
 *
 *\note		  
 */
void printBootromLogo()
{
   
    char buf[100];

	char* time = __DATE__;

	int year = 0;

	int i = 0;


	for (i = strlen(time) - 1; i > 0 ; i--)
	{
		if (time[i] == 32)
		{
			break;
		}	
	}	

	year = simple_strtoul(&time[i + 1], NULL, 10);

    sprintf(buf, "Create Date: %s - %s\n", __DATE__, __TIME__);

    printf("\t\t%s","*********************************************\n");
    printf("\t\t%s%s%s","*         TP-LINK  BOOTUTIL(v",BOOT_VERSION,")         *\n");
    printf("\t\t%s","*********************************************\n");
    printf("\t\t%s%d%s","Copyright (c) ", year, " TP-LINK Tech. Co., Ltd    \n");
    printf("\t\t%s\r\n",buf);


}  


/*!
 *\fn			bool cmdHelp(char *param)
 *\brief   
 *
 *\param[in]   
 *\param[out]  
 *
 *\return  
 *\retval  
 *
 *\note		  
 */
 
bool cmdHelp(void)
{
	int i = 0;

	int cmdNumber = sizeof(bootCommand) / sizeof(cmdStruct) - 1;
	
	
	printf("   Boot Menu\r\n");
	
	for (i = 0; i < cmdNumber; i++)
	{
		printf("%-2d %s\n", i, bootCommand[i].description);
	}
    
	printf("\r\nEnter your choice(0-%d)\r\n", cmdNumber-1);
	
	printf ("\n");

	return TRUE;
}


bool cmdReboot(void)
{
    char line[BUF_LINE] = {0};

    printf("Are you sure to reboot the device?[Y/N]:");
    cmdReadLine(line, NULL);
	
    if (tolower(line[0]) != 'y')
    {
        return TRUE;
    }

    printf("rebooting...\r\n");
    mdelay(1000);    /* pause a second */
    sysReboot();
    return TRUE;
}


bool cmdReset(void)
{
    char line[BUF_LINE] = {0};

    printf("Are you sure to reset the device?[Y/N]:");
  	cmdReadLine(line, NULL);

    if (tolower(line[0]) != 'y')
    {
        return TRUE;
    }
    setEnvItem(TP_SWITCH_START_CONFIG, "NULL");
    printf("resetting...\r\n");
    mdelay(1000);    /* pause a second */
    sysReboot();
    
    return TRUE;
}

bool cmdStart(void)
{
    #if 0
    char command[256];
    int nRet = 0;

    memset(command, 0, sizeof(command));
    sprintf(command, "bootm 0xb4100000");
    nRet = run_command(command, 0);
    if(1 == nRet )
    {
        printf("The kernel has been damaged!\nbegin to fix kernel...\n");

        return FALSE;
    }
    printf("%s,line%d \n\r",__FUNCTION__,__LINE__);
    return TRUE;
    #else
    //ulong img_addr = 0;
    char command[CONFIG_SYS_CBSIZE];
    char value[ENV_ITEM_VALUE_LEN] = {0};
    char tmp[BUF_LINE] = {0};
    int nRet = 0;
	int baudrate = 0;
	
	baudrate = getenv_ulong("baudrate", 10, 38400);

    
    flashAppInit();
    getEnvItem(TP_SWITCH_START_CONFIG, value);
    if(strcmp(value, "NULL") == 0)
    {
        char * configarea = NULL;
        configarea = getenv("config_area");
        if(strcmp(configarea, "null") != 0)
        {
            setenv("config_area", "null");
            saveenv();           
        }
         setEnvItem(TP_SWITCH_START_CONFIG, TP_SWITCH_CONFIG_NAME_1);
    }
    else
    {
        char * configarea = NULL;
        configarea = getenv("config_area");
        if(strcmp(configarea, TP_SWITCH_CONFIG_NAME_1) != 0)
        {
            setenv("config_area", TP_SWITCH_CONFIG_NAME_1);
            saveenv();
        }
    }  

    if (FALSE == imageFileExist(getStartUpIndex()))
    {
        printf("The Startup Image does not exist\r\n");
        //return FALSE;
    }

    /*启动前需要检查更新启动区域*/
    int index = getStartUpIndex();
    int curArea = 1;
    char * pArea = NULL;
    pArea = getenv("bootarea");

    curArea = pArea[0] - '1' + 1;

    sysSetActiveInfoWithOutFlash(index, baudrate);/*去掉不必要的save操作*/
    sprintf(tmp, "%d", index);
    setenv("bootarea", tmp);	


    printf("\nBegin to startup system, please wait a moment...\n");

    memset(command, 0, sizeof(command));
    sprintf(command, "bootm 0x%x", IMAGE_ADDR);
    nRet = run_command(command, 0);
    if(1 == nRet )
    {
        nRet = AutoFixKernel();
        if(ERROR == nRet)
        {
            return FALSE;
        }
    }
    
    return TRUE;
    #endif
}



bool cmdActivateBackupImage(void)
{
    char line[BUF_LINE] = {0};
    ulong nRegion = 1;

    	
    if (1 == getStartUpIndex())
    {
    	    nRegion = 2;
    }
    else
    {
    	    nRegion = 1;
    	
    }	

    if (FALSE == imageFileExist(nRegion))
    {
        	printf("The Backup Image does not exist in system\r\n");
        	return FALSE;
    }
    
    /* 设置全局变量 */
    printf("Are you sure to set the Backup Image as Startup Image?[Y/N]:");
	line[0] = 0;
    cmdReadLine(line, NULL); 
    if (tolower(line[0]) == 'y')
    {
		if(1 == getStartUpIndex())
		{
			setStartUpIndex(2);
		}
		else
		{
			setStartUpIndex(1);
		}
				 
        /* 显示文件列表 */
        cmdShowImage();
		
    }
    printf("\r\n");
    return TRUE;
}

bool cmdShowImage(void)
{ 
    #if 1
	//image_header_t* phdr = NULL;
	int index = 0;

	ulong img_len = 0;
	//uint32_t time = 0;
       char cImgName[16] = {0};
       char * pImgName = "NULL";

	char* startUpLabel = "(*)"; 
	char* backUpLabel = "(b)";

	printf("Images in system:\r\n");
	printf("index  Attribute  Size        Filename\r\n");
	printf("-----  ---------  ----------  ---------------------\r\n");

	for (index = 0; index < sizeof(imageName)/ sizeof(char*); index ++)
	{
            char* label = NULL;
            if (FALSE == imageFileExist(index + 1))
            {
                img_len = 0;
                pImgName = "NULL";
            }
            else
            {
                sprintf(cImgName, "usrimg%d", (index+1));  
                img_len = (int)sysGetMtdpartsInfo((char *)cImgName);
                pImgName = imageName[index];
            }
            if (index + 1 == getStartUpIndex())
            {
                label = startUpLabel;
            }
            else
            {
                label = backUpLabel;
            }	

            printf("%-5d  %-9s  %-10d  %-21s\r\n",
								index + 1, label, (int)img_len,
								pImgName);
	}
	
    printf("-----  ---------  ----------  ---------------------\r\n");
    printf("\r\n");
    printf("(*) - with the Startup attribute\r\n");
    printf("(b) - with the Backup attribute\r\n");	
    #endif
	return TRUE;    
    
}

bool cmdPwdRecovery(void)
{
    UINT8 gevValue[512] = {0};
    int pwdEnable = 0;
    char line[BUF_LINE] = {0};

    printf("This will delete all the previously created accounts. Continue?[Y/N]:");
    cmdReadLine(line, NULL);
	
    if (tolower(line[0]) != 'y')
    {
        return TRUE;
    }
    
    /* 密码恢复功能开关判断 */
    if (ERROR == getEnvItem(TP_SWITCH_PWD_RECOVERY, (char *)gevValue))
    {
        printf("Fatal error: Failed to get password recovery status.\r\n");
        return FALSE;
    }
    pwdEnable = simple_strtoul((const char *)gevValue, NULL, 10);
    if (pwdEnable == 0)
    {
        printf("Password Recovery function is disabled.\r\n");
        return TRUE;
    }

    /* 设置密码恢复标志位 */
    if (ERROR == setEnvItem(TP_SWITCH_PWD_RECOVERY_FLAG, "1"))
    {
        printf("Fatal error: Failed to set password recovery flag.\r\n");
        return FALSE;
    }
    printf("Operation OK!\r\n");
    
    return TRUE;
}
 
#ifdef UBOOT_DEBUG

 /**************************************************************
 * Function  : swChkDotIpAddr()
 * Abstract  : To check if the input dot IP is legal
 * Input     : strIp - IP address in dot format
 * Output    : N/A
 * Return    : TRUE - legal, FALSE - illegal
 */
 static bool swChkDotIpAddr(unsigned char *strIp)
 {
     UINT8*  pt = strIp;
     UINT16   ipFirst=0;
     UINT16   nTemp = 0;
     UINT16   nPosition = 0;
 
     if(pt == NULL)
     {
         return FALSE;
     }
 
     if (strlen((const char*)strIp) < 7 || strlen((const char*)strIp) > 15)
     {
         return FALSE;
     }
 
     while(*pt != '\0')
     {
         switch(*pt)
         {
             case '0':        case '1':
             case '2':        case '3':
             case '4':        case '5':
             case '6':        case '7':
             case '8':        case '9':
 
             nTemp = nTemp * 10 + *pt - '0';
             if(nTemp > 255)
             {
                 return FALSE;
             }
             break;
 
             case '.':
             if(*(pt + 1) == '.')
             {
                 return FALSE;
             }
 
             if(nPosition==0)
             {
                 ipFirst = nTemp;
             }
             
             nPosition ++;
             nTemp = 0;
             
             break;
 
             default:
                 return FALSE;
         }
         pt++;
     }
 
     if(nPosition != 3)
     {
         return FALSE;
     }
 
     return TRUE;
 }
 
  /**************************************************************
  * Function : swChkLegalIpAddr()
  * Abstract : To check if the input value of IP address is normal IP address
  * Input    : ulIp - (This IP must be in big-endian format)
  * Output   : N/A
  * Return   : TRUE - Legal, FALSE - Illegal
  */
 static bool swChkLegalIpAddr(UINT32 ulIp) 
 {
     UINT32 ipAddr = ulIp;   
     /* The hostid of IP address can not be all 0 or all 1*/
     /*modify for A/B/C type ip address check together
     */
     if ((ipAddr & 0xF0000000) >= 0xe0000000 
         || (ipAddr & 0xFF000000) == 0 /*D类(1110) E类(1111)*/
         || ipAddr == 0x7F000001) /*环回,127.0.0.1*/
     {
         printf("Ip check error!\n\r");
         return FALSE;
     }
 
     if( (ipAddr & 0x000000FF) == 0xFF 
         || (ipAddr & 0x000000FF) == 0x00)
     {
         return FALSE;
     }
         
     return TRUE;
 }


 /**************************************************************
 * Function	: maskCheck()
 * Abstract : To check if the input value of MASK is legal
 * Input  	: ulMask
 * Output	: N/A
 * Return	: TRUE - legal, FALSE - illegal
 */
static bool maskCheck(char* param, UINT32 *mask)
{
	int i;
	UINT32 ulTmp,ulCmp ;
	UINT32 ulMask;

	ulCmp = (UINT32)0xFFFFFFFF;
	ulTmp = (UINT32)0x00000001;


	if( NULL == param)
	{
		return FALSE;
	}

	if( FALSE == swChkDotIpAddr((unsigned char *)param) )
	{
		return FALSE;
	}

	ulMask = (UINT32)string_to_ip(param);

	ulMask = ntohl(ulMask);
	
	if (ulMask == 0x00)
	{
		return FALSE;
	}

	for (i = 0; i < 32; i++)
	{
		if ((ulTmp & ulMask) != 0x00)
		{
			if(ulMask == (ulCmp << i))
			{
				*mask = ulMask;
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			ulTmp = ulTmp << 1;
		}
	}
	return FALSE;	
}

/*!
 *\fn			bool ipCheck(char *param)
 *\brief   		check the ip address valid or not
 *
 *\param[in]   	param	the dot format address
 *\param[out]  	N/A
 *
 *\return  		TRUE	valid
 				FALSE 	invalid
 *\retval  
 *
 *\note		  
 */
static bool ipCheck(char *param)
{
    UINT32 ulMask;
	
	if( NULL == param)
	{
		return FALSE;
	}

	if(FALSE == swChkDotIpAddr((unsigned char *)param) )
	{
		return FALSE;
	}

    ulMask = (UINT32)string_to_ip(param);

	ulMask = ntohl(ulMask);
	
	if(FALSE == swChkLegalIpAddr(ulMask))
	{
		return FALSE;
	}

	return TRUE;
}



bool cmdUpdate(void)
{
    char *buffer = NULL;
    int maxSize = FLASH_SIZE;
    int recSize = 0;
    char* pline = NULL;
    char line[BUF_LINE] = {0};
    int saveIndex = 0;
    int choiceIndex = 0;
    bool rc = FALSE;
    char downFile[BUF_LINE] = {0};
    IMAGE_INFO	stImageInfo;

    memset(&stImageInfo, 0, sizeof(IMAGE_INFO));

    printf("You can only use the port 1 to upgrade.\r\n");
	printf("You can press CTRL-C to stop loading.\r\n");

    sprintf(downFile, "%s:%s", getenv("serverip"), getenv("bootfile"));

    /* 获取文件 */
    if( FALSE == ftpGetImage(&buffer, downFile, &recSize, maxSize))
    {
        printf("Download failed\r\n");
        return FALSE;
    }

    if (0 == recSize)
    {
        printf("Download failed\r\n");
        return FALSE;		
    }	
	
    /* 保存文件 */
    printf("Specify the image name in system:\r\n");
    printf("1 - %s\r\n", imageName[0]);
    printf("2 - %s\r\n", imageName[1]);
    printf("0 - cancel and return\r\n");
SPECIFY_FN:
    printf("Enter your choice (0-2):");
	line[0] = 0;
    cmdReadLine(line, NULL); 
    pline = line;
    skipSpace(&pline);
	
    if (IS_NOT_NEW_LINE(pline))
    {
        if (IS_NOT_DIGITAL_LINE(pline))
		{
            ERR_NOT_DIGITAL_PRINT();
            rc = FALSE;
            goto RET;
		}
        saveIndex = simple_strtoul(pline, NULL, 10);
		
        if ((saveIndex < 0) || (saveIndex > 3))
        {
            printf("Invalid choice\r\n");
            rc = FALSE;
            goto SPECIFY_FN;
        }

        if(saveIndex == 0)
        {
            rc = TRUE;
            goto RET;
        }

    }
    else
    {
        goto SPECIFY_FN;
    }

     /* 判定分区是否是变小，如果变小需要确认是否强制升级*/
    if(ERROR == sysParseImage((const unsigned char*)buffer, recSize, &stImageInfo))
    {
        printf("sysParaImage fail\n");
        return FALSE;
    }

    /*将镜像写入分区*/
    if (FALSE == bootWriteFile((const char*)buffer, recSize, saveIndex, stImageInfo))
    {
        printf("Failed to write file");
    }
    printf("\r\n");

    /* 设置属性 */
    printf("Specify the attribute of the image file(%s):\r\n", imageName[saveIndex - 1]);
    printf("1 - Startup Image\r\n");
    printf("2 - Backup Image\r\n");
    printf("0 - use default\r\n");
	
SPECIFY_ATTRIBUTE:
    printf("Enter your choice (0-2):");
	line[0] = 0;
    cmdReadLine(line, NULL); 
    pline = line;
    skipSpace(&pline);
	
    if (IS_NOT_NEW_LINE(pline))
    {
        if (IS_NOT_DIGITAL_LINE(pline))
		{
            ERR_NOT_DIGITAL_PRINT();
            rc = FALSE;
            goto RET;
		}
		
	 choiceIndex = simple_strtoul(pline, NULL, 10);
        if ((choiceIndex < 0) || (choiceIndex > 3))
        {
            printf(" Invalid choice\r\n");
            goto SPECIFY_ATTRIBUTE;
        }

        switch(choiceIndex)
        {
        case 0:
			rc = TRUE;
            break;
        case 1:
			if (1 == saveIndex)
			{
				setStartUpIndex(1);
			}
			else
			{
				setStartUpIndex(2);
			}
            rc = TRUE;
            break;
        case 2:
			if (1 == saveIndex)
			{
				setStartUpIndex(2);
			}
			else
			{
				setStartUpIndex(1);
			}	
            rc = TRUE;
            break;
        default:
            goto SPECIFY_ATTRIBUTE;
        }
    }
    else
    {
		
        goto SPECIFY_ATTRIBUTE;
    }

RET:
    printf("\r\n");
	/* 显示文件列表 */
    cmdShowImage();
    return rc;

} 


static bool changeEndianness(char * pImageBuff, int len, int endian)
{
	char buff[4];
	
	//char* pImage = NULL;
	
	int index = 0;

	if(NULL == pImageBuff || len <= 0)
    {
        return ERROR;
    }
	
	if(0 !=  (len % 4) )
	{
		printf("The size of softWare must be 4 bytes alligned\r\n");
		return ERROR;
	}
	
	if (1 > endian || 4 < endian)
	{
		return ERROR;
	}
	
	if (1 == endian)
	{
		return OK;
	}
	
	for (index = 0; index < (len/4); index ++)
	{
		memcpy(buff, &pImageBuff[4 * index], 4);
		
		if(2 == endian)
		{
			pImageBuff[4 * index] = buff[3];
			pImageBuff[4 * index + 1] = buff[2];
			pImageBuff[4 * index + 2] = buff[1];
			pImageBuff[4 * index + 3] = buff[0];
		}
		else if (3 == endian)
		{
			pImageBuff[4 * index] = buff[2];
			pImageBuff[4 * index + 1] = buff[3];
			pImageBuff[4 * index + 2] = buff[0];
			pImageBuff[4 * index + 3] = buff[1];			
		}
		else if (4 == endian)
		{
			pImageBuff[4 * index] = buff[1];
			pImageBuff[4 * index + 1] = buff[0];
			pImageBuff[4 * index + 2] = buff[3];
			pImageBuff[4 * index + 3] = buff[2];			
		}		
	}

	return OK;
}

bool cmdLoadSoftWare(void)
{
    char *buffer = NULL;
    int maxSize = FLASH_SIZE;
    int recSize = 0;
    char* pline = NULL;
    char line[BUF_LINE] = {0};
    int choiceIndex = 0;
    bool rc = FALSE;
    char downFile[BUF_LINE] = {0};
	


    printf("You can only use the port 1 to upgrade.\r\n");
	printf("You can press CTRL-C to stop loading.\r\n");

    sprintf(downFile, "%s:%s", getenv("serverip"), getenv("bootfile"));

    /* 获取文件 */
    if( FALSE == ftpGetImage(&buffer, downFile, &recSize, maxSize))
    {
        printf("Download failed\r\n");
        return FALSE;
    }

    if (0 == recSize)
    {
        printf("Download failed\r\n");
        return FALSE;		
    }	
	
    /* 保存文件 */
	printf("Please select Endianness option\r\n");
    printf("1 - 1234->1234(default)\r\n");
	printf("2 - 1234->4321(may be used in further)\r\n");
	printf("3 - 1234->3412(may be used in further)\r\n");
	printf("4 - 1234->2143(may be used in further)\r\n");
    printf("0 - cancel and return\r\n");
SPECIFY_FN:
    printf("Enter your choice (0-4):");
	line[0] = 0;
    cmdReadLine(line, NULL); 
    pline = line;
    skipSpace(&pline);
	
    if (IS_NOT_NEW_LINE(pline))
    {
        if (IS_NOT_DIGITAL_LINE(pline))
		{
            ERR_NOT_DIGITAL_PRINT();
            rc = FALSE;
            goto RET;
		}
        choiceIndex = simple_strtoul(pline, NULL, 10);
		
        if ((choiceIndex < 0) || (choiceIndex > 4))
        {
            printf("Invalid choice\r\n");
            rc = FALSE;
            goto SPECIFY_FN;
        }

        if(choiceIndex == 0)
        {
            rc = TRUE;
            goto RET;
        }

    }
    else
    {
        goto SPECIFY_FN;
    }
	
	
	if (ERROR == changeEndianness(buffer, recSize, choiceIndex))
	{
		return FALSE;
	}

    if(ERROR == sysLoadSoftWare(buffer, recSize))
    {
        printf("Load software fail.Please reload the uboot\n");
        return FALSE;
    }
	
	printf("\r\n");
	sysReboot();
	return rc;

RET:
	
    printf("\r\n");
    return rc;

} 



/*!
 *\fn			 int tftpDownloadFile(unsigned int dlAddr, char* pFileName, unsigned int maxSize)
 *\brief			 Download the kernel image and usriamg.jffs2 image ,and start it from 
 *				   RAM directly.
 *\param[in]		pFileName : 下载文件名
 *\param[in]		dlAddr   : 下载到RAM的地址
 *\param[in]		maxSize   : 下载文件的最大大小
 *\return			返回: 大于0: 下载文件大小，
 *		                小于0: 下载出错
 *\note			added by zengjunjun
 */
static  int tftpDownloadFile(unsigned int dlAddr, char* pFileName, unsigned int maxSize)
{
    //char *pBuf;				
    char command[CONFIG_SYS_CBSIZE] = {0};
    int flag = 0;
    int dlLen = 0;
    int rv = 0;
    if (NULL == pFileName )
    {
        return -1;
    }    
   
    sprintf(command, "tftpboot 0x%0x %s 0x%0x 0x%0x", dlAddr, pFileName, (unsigned int)(&dlLen), maxSize);
    rv= run_command(command, flag) ;
    if( rv != 0 )
    {
	 return -2;
    }
    
    if(  dlLen == 0 || dlLen > maxSize)
    {
	return -3;
    }
	
    return dlLen;
	
}
  
/*!
 *\fn			 int tftpUploadFile(unsigned int dlAddr, char* pFileName, unsigned int maxSize)
 *\brief			 Upload file from switch 
 *				   RAM directly.
 *\param[in]		pFileName : 上载文件名
 *\param[in]		dlAddr   : 上载到RAM的地址
 *\param[in]		maxSize   : 上载文件的最大大小
 *\return			返回: 大于0: 上载文件大小，
 *		                小于0: 上载出错
 *\note			added by zengjunjun
 */
static  int tftpUploadFile(unsigned int dlAddr, char* pFileName, unsigned int maxSize)
{
    //char *pBuf;				
    char command[CONFIG_SYS_CBSIZE] = {0};
    int flag = 0;
    int rv = 0;
    if (NULL == pFileName )
    {
        return -1;
    }    
   
    sprintf(command, "tftpput 0x%0x  0x%0x %s", dlAddr, maxSize, pFileName);
    rv= run_command(command, flag) ;
    if( rv < 0 )
    {
        printf("upload cmd fail\n");
	 return -2;
    }
    
    return 0;
	
}


/*!
 *\fn			 bool cmdStartImageFromRam(void)
 *\brief			 Download the kernel image + usriamg.jffs2 image ,and start it from RAM directly.
 *\param[in]		 N/A
 *\param[out]		 N/A
 *\return			 TURE or FALSE
 *\note			 added by zengjunjun
 */
 bool cmdStartImageFromRam(void)
{
	char downFile[BUF_LINE] = {0};
	char command[CONFIG_SYS_CBSIZE] = {0};
	int dlSize = 0;
	unsigned int padStart = 0;
	unsigned char *pProductId = NULL;

	/* 下载productID */
	sprintf(downFile, "%s:productId", getenv("serverip"));
	dlSize = tftpDownloadFile(TP_KERNEL_IMAGE_DL_ADDR, downFile, TP_KERNEL_IMAGE_MAX_SIZE);
	if(dlSize < 0 )
	{
		printf("Download failed\r\n");
		printf("\r\nTips: have you set the tftp server directory to ../buildroot/image/source/ ?\r\n\r\n");
        	return FALSE;
	}
	pProductId = (unsigned char *)TP_KERNEL_IMAGE_DL_ADDR;
	flashAppInit();
	flashProbe();
	/* 检查productID */
	if (OK != sysFirmwareCheckProduct((const unsigned char *)pProductId))
	{
	    printf("\r\nError: Verifing productID error!\r\n");
	    printf("%s","Please check whether the image can work on this switch, \r\n"
			"if not, download the correct image or update the switch's profile.\r\n");
	    return FALSE;
	}

	/* 检查启动镜像 */
	 if (1 != getStartUpIndex())
	{
		setStartUpIndex(1);
		printf("[Warning]:The Startup Image is not %s. To support RAM Startup,\r\n" 
			"          we haved change the Startup Image to %s.\r\n",TP_SWITCH_IMAGE_NAME_1,
			TP_SWITCH_IMAGE_NAME_1); 
	}

	/* 下载内核 */ 
	sprintf(downFile, "%s:%s", getenv("serverip"), TP_KERNEL_IMAGE_NAME);
	dlSize = tftpDownloadFile(TP_KERNEL_IMAGE_DL_ADDR, downFile, TP_KERNEL_IMAGE_MAX_SIZE);
	if(dlSize < 0 )
	{
		printf("Download failed\r\n");
        	return FALSE;
	}

	/* 下载用户镜像 */
	sprintf(downFile, "%s:%s", getenv("serverip"), TP_USER_IMAGE_NAME);
	
	dlSize = tftpDownloadFile(TP_USER_IMAGE_DL_ADDR, downFile, TP_USER_IMAGE_MAX_SIZE);
	if(dlSize < 0 )
	{
		printf("Download failed\r\n");
        	return FALSE;
	}
	
	/* PAD 0xFF */
	padStart = (TP_USER_IMAGE_DL_ADDR + dlSize + 3) & 0xfffffffc; /* 4bytes align */
	memset((void *) padStart, 0xff, ( TP_USER_IMAGE_DL_ADDR + TP_USER_IMAGE_MAX_SIZE - padStart )) ;
	
	/* OK, Run!! */
	printf("\nBegin to startup system from RAM, please wait a moment...\r\n");

	sprintf(command, "bootm 0x%x", TP_KERNEL_IMAGE_DL_ADDR);
	run_command(command, 0);
	
	return TRUE;
}


bool cmdUploadInner(void)
{
    int nRet = ERROR;
    char downFile[BUF_LINE] = {0};
    int InnerOffset = sysGetFlAddr("para");
    int InnerSize = FLASH_SIZE - InnerOffset ;

    if(InnerOffset > FLASH_SIZE)
        return FALSE;

    
    printf("Begin to upload the Inner from 0x%x....\n", InnerOffset);

    readFlash((char *)TP_KERNEL_IMAGE_DL_ADDR, InnerSize, InnerOffset);
    
    sprintf(downFile, "%s:Inner", getenv("serverip"));
    nRet = tftpUploadFile(TP_KERNEL_IMAGE_DL_ADDR, downFile, InnerSize);
    if(nRet < 0 )
    {
    	printf("upload Inner failed\r\n");
       return FALSE;
    }

    printf("Inner OK!\r\n");
    return TRUE;
}

/*!
 *\fn			bool cmdIfconfig(void)
 *\brief
 *
 *\param[in]
 *\param[out]
 *
 *\return
 *\retval
 *
 *\note
 */
bool cmdIfconfig(void)
{
	int ipStatus = FALSE;
    int maskStatus = FALSE;
    int gatewayStatus = FALSE;
    char ipAddr[IP_ADDRESS_LENGTH] = {0};
    char routeAddr[IP_ADDRESS_LENGTH] = {0};
    unsigned int netMask = 0;
	BOOT_CONFIG bootParam;

	
    char* pline = NULL;
    char line[BUF_LINE] = {0};


	strcpy(bootParam.ipAddr, getenv("ipaddr"));
    printf("  Ip Address (%s):", bootParam.ipAddr);
	cmdReadLine(line, NULL); 
    pline = line;
    skipSpace(&pline);
	
    if (IS_NOT_NEW_LINE(pline))
    {
        if (FALSE == ipCheck(pline))
        {
            printf("The ip is invalid\r\n");
            return FALSE;
        }
        strncpy(ipAddr, pline, IP_ADDRESS_LENGTH);
        memset(pline, 0, sizeof(pline));
        ipStatus = TRUE;
    }

    strcpy(bootParam.ipMask, getenv("netmask"));
    printf("  Ip Mask (%s):", bootParam.ipMask);
    cmdReadLine(line, NULL); 
    pline = line;
    skipSpace(&pline);

	
    if (IS_NOT_NEW_LINE(pline))
    {
        if (FALSE == maskCheck(pline, &netMask))
        {
            printf("The mask is invalid\r\n");
            return FALSE;
        }
		netMask = htonl(netMask);
        memset(pline, 0, sizeof(pline));
        maskStatus = TRUE;
    }

	strcpy(bootParam.routeAddr, getenv("gatewayip"));
    printf("  Gateway (%s):", bootParam.routeAddr);
	line[0] = 0;
    cmdReadLine(line, NULL);  
    pline = line;
    skipSpace(&pline);

    if (IS_NOT_NEW_LINE(pline))
    {
        if (FALSE == ipCheck(pline))
        {
            printf("The gateway is invalid\r\n");
            return FALSE;
        }

        strncpy(routeAddr, pline, IP_ADDRESS_LENGTH);
        memset(pline, 0, sizeof(pline));
        gatewayStatus = TRUE;
    }

    if (!ipStatus)
    {
        strncpy(ipAddr, bootParam.ipAddr, IP_ADDRESS_LENGTH);
    }

    if (!gatewayStatus)
    {
        strncpy(routeAddr, bootParam.routeAddr, IP_ADDRESS_LENGTH);
    }
    if (!maskStatus)
    {
        netMask = string_to_ip(bootParam.ipMask);
    }

    if ((ipStatus == TRUE) || (gatewayStatus == TRUE) || (maskStatus == TRUE))
    {
        /* config the interface and set the parameters to flash */
		setenv("ipaddr", ipAddr);
		setenv("gatewayip", routeAddr);
		ip_to_string(netMask, bootParam.ipMask);
        setenv("netmask", bootParam.ipMask);   
		saveenv();
        printf("\r\n");
        return FALSE;
    }
    else
    {
        printf("\r\n");
        return TRUE;
    } 

    return FALSE;
}


bool cmdTftp(void)
{
    char* pline = NULL;
    char line[BUF_LINE] = {0};
    bool isChanged = FALSE;
	BOOT_CONFIG bootParam;


	strcpy(bootParam.hostAddr, getenv("serverip"));
    printf("  Tftp Ip Address (%s):", bootParam.hostAddr);
    cmdReadLine(line, NULL); 
    pline = line;
    skipSpace(&pline);
	
    if (IS_NOT_NEW_LINE(pline))
    {
        if (FALSE == ipCheck(pline) )
        {
            printf("The host ip is invalid\r\n");
            return FALSE;
        }
        strncpy(bootParam.hostAddr, pline, BOOT_ADDR_LEN);
        isChanged = TRUE;
    }


	strcpy(bootParam.fileName, getenv("bootfile"));
    printf("  Tftp fileName (%s):", bootParam.fileName);
	line[0] = 0;
  	cmdReadLine(line, NULL);   
    pline = line;
    skipSpace(&pline);
	
    if (IS_NOT_NEW_LINE(pline) &&
        (strncmp(bootParam.fileName, pline, BOOT_FILE_LEN) != 0))
    {
        strncpy(bootParam.fileName, pline, BOOT_FILE_LEN);
        printf("bootParam.fileName is %s\r\n", bootParam.fileName);
        isChanged = TRUE;
    }

    if (isChanged)
    {
 		setenv("serverip", bootParam.hostAddr);
        setenv("bootfile", bootParam.fileName);   
		saveenv();      
    }

    printf("\r\n");
    return TRUE;
}

bool cmdDeleteBackupImage(void)
{
	//char* pline = NULL;
	char line[BUF_LINE] = {0};
	//ulong img_addr = 0;
       int nIndexRegion = 0;

	
	/* 列出镜像文件 */
	cmdShowImage();

	printf("Are you sure to delete the Backup Image file(%s)?[Y/N]:", imageName[((1 == getStartUpIndex() ? 2:1) + 1) % 2]);
	line[0] = 0;
	cmdReadLine(line, NULL); 
	if (tolower(line[0]) == 'y')
	{
		/*将对应区的内容擦除*/
              nIndexRegion = (1 == getStartUpIndex() ? 2:1);
              sysEraseImage(nIndexRegion);  
	}

	printf("\r\n");
	return TRUE;

}

bool cmdUpdateCfg(void)
{

    int nRet = OK;
    //char* pline = NULL;
    char line[BUF_LINE] = {0};
    char *buffer = NULL;
    int maxSize = FLASH_SIZE;
    int recSize = 0;
    char downFile[BUF_LINE] = {0};

    sprintf(downFile, "%s:usrapp.jffs2", getenv("serverip"));

    /* 获取文件 */
    if( FALSE == ftpGetImage(&buffer, downFile, &recSize, maxSize))
    {
        printf("Download failed\r\n");
        return FALSE;
    }

    if (0 == recSize)
    {
        printf("Download failed\r\n");
        return FALSE;		
    }	

    printf("Are you sure to update the configure file?[Y/N]:");
    line[0] = 0;
    cmdReadLine(line, NULL); 
    if (tolower(line[0]) == 'y')
    {
        /*将对应区的内容更新*/
        nRet = sysUpdateCfg(buffer, recSize);
        if(OK != nRet)
        {
            printf("UpdateCfg fail!\r\n");
        }
    }

    printf("\r\n");
    return TRUE;

}

bool cmdDeleteCfg(void)
{

    //char* pline = NULL;
	char line[BUF_LINE] = {0};
	//ulong img_addr = 0;
    //int nIndexRegion = 0;

	printf("Are you sure to delete the configure file?[Y/N]:");
	line[0] = 0;
	cmdReadLine(line, NULL); 
	if (tolower(line[0]) == 'y')
	{
		/*将对应区的内容擦除*/
              sysEraseCfg();  
	}

	printf("\r\n");
	return TRUE;

}


bool cmdExitMenu(void)
{
    g_ExitMenuFlag = 1;
    return TRUE;
}


LOCAL bool cmdProfile(void)
{
    char line [BUF_LINE] = {0};
    char *pImage = NULL;
    int  imageLength = 0;
    int maxSize = FLASH_SIZE;
    bool rc = FALSE;
    char downFile[BUF_LINE] = {0};

    sprintf(downFile, "%s:profile", getenv("serverip"));

    flashAppInit();

    /* 获取文件 */
    if( FALSE == ftpGetImage(&pImage, downFile, &imageLength, maxSize))
    {
        printf("Download failed\r\n");
        return FALSE;
    }

    if (0 == imageLength)
    {
        printf("Download failed\r\n");
        return FALSE;		
    }	

    printf("Are you sure to upgrade the profile?[Y/N]:");
    cmdReadLine(line, NULL);

    if (tolower(line[0]) != 'y')
    {
        rc = TRUE;
        goto RET;
    }

    flashProbe();

    //flashErase(0x1f00000/FLASH_SECTOR_SIZE, 0x100000/FLASH_SECTOR_SIZE);
    
    //resetEnv();

    if (ERROR == flashAppInnerSet(FLASH_INNER_ITEM_PROFILE, pImage, imageLength, TRUE))
    {
        rc = FALSE;
    }
    else
    {
        printf("\nUpdgrade the profile ok!\n");
        rc = TRUE;
        sysReboot();
    }
RET:

    printf("\r\n");
    return rc;
}

LOCAL bool cmdUpdateUboot(void)
{
    char line [BUF_LINE] = {0};
    char *pImage = NULL;
    int  imageLength = 0;
    int maxSize = FLASH_SIZE;
    bool rc = FALSE;
    char downFile[BUF_LINE] = {0};

    sprintf(downFile, "%s:u-boot.bin", getenv("serverip"));

    /* 获取文件 */
    if( FALSE == ftpGetImage(&pImage, downFile, &imageLength, maxSize))
    {
        printf("1 Download failed\r\n");
        return FALSE;
    }

    if (0 == imageLength)
    {
        printf("2 Download failed\r\n");
        return FALSE;		
    }	

    printf("Are you sure to upgrade the u-boot?[Y/N]:");
    cmdReadLine(line, NULL);
    //printf("%s:%d\n",__FUNCTION__,__LINE__);
    if (tolower(line[0]) != 'y')
    {
        printf("%s:%d\n",__FUNCTION__,__LINE__);
        rc = TRUE;
        goto RET;
    }

    if (ERROR == sysUpdateUboot(pImage, imageLength))
    {
        printf("%s:%d\n",__FUNCTION__,__LINE__);
        rc = FALSE;
    }
    else
    {
        rc = TRUE;
        printf("\nUpgrade the u-boot ok\n");
        sysReboot();
    }
RET:
    printf("%s:%d\n",__FUNCTION__,__LINE__);

    printf("\r\n");
    return rc;

}

#include "spi_flash.h"
static int seedp = 0;
int srand_sftest(int s)
{
    seedp = s;
    return TRUE;
}
unsigned int rand_sftest(void)
{
	seedp ^= (seedp << 13);
	seedp ^= (seedp >> 17);
	seedp ^= (seedp << 5);
    return seedp;
}
LOCAL bool cmdFlashTest(void)
{
    #if 0
    char line [BUF_LINE] = {0};
    unsigned long start = 0L;
    unsigned long cost = 0L;
    unsigned char* buf1 = NULL;
    unsigned char* buf2 = NULL;
    int testOffset = 0;
    int testLen = 0;
    bool rv = TRUE;
    struct spi_flash * flash = NULL;
    int i = 0;

    printf("This command will override the flash except the low 1M, and it will reboot after the test.\r\n");
    printf("Do you really want to do this?[Y/N]:");
    cmdReadLine(line, NULL);

    if (tolower(line[0]) != 'y')
    {
        return TRUE;
    }

    flash = spi_flash_probe(0, 0, 0X1000000, 0X3);
    if (NULL == flash)
    {
        printf("Failed to attach SPI flash.\r\n");
        return TRUE;
    }
    printf("Attach flash OK. Flash info:\r\n");
    printf("Name:   %s\r\n", flash->name);
    printf("Size:   %#x\r\n", flash->size);
    printf("Sector: %#x\r\n", flash->sector_size);
    printf("Page:   %#x\r\n\r\n", flash->page_size);

    if ((flash->size < 0x100000)
        || (flash->size > 0x2000000))
    {
        printf("Invalid flash of size. Test abort.\r\n");
        return TRUE;
    }
    else
    {
        testOffset = 0x100000;  /* Skip the low 1M to avoid modifying the u-BOOT */
        testLen = flash->size - testOffset;
        printf("Test from %#x to %#x of the flash.\r\n", testOffset, flash->size);
        printf("Prepare for testing...\r\n");
        /* buf1 = (unsigned char*)calloc(testLen, 1); */
        /* buf2 = (unsigned char*)calloc(1000, 1); */
        buf1 = 0x61000000;
        buf2 = 0x63000000;
        if ((buf1 == NULL) || (buf2 == NULL))
        {
            printf("Test abort for allocing memory failed.\r\n");
            goto RET;
        }

        srand_sftest(get_timer(0));
        for (i = 0; i < testLen; i++)
            *((unsigned int*)buf1) = rand_sftest();
        printf("Prepare done\r\n\r\n");
    }

    printf("Test erasing...\r\n");
    start = get_timer(0);
    rv = flash->erase(flash, testOffset, testLen);
    if (rv != 0)
    {
        printf("Test abort for erasing failed.\r\n");
        goto RET;
    }
    cost = get_timer(start);
    printf("Erase done. Cost %lds to erase %dMB\r\n\r\n", cost/400, testLen>>20);

    printf("Test writing...\r\n");
    start = get_timer(0);
    rv = flash->write(flash, testOffset, testLen, buf1);
    if (rv != 0)
    {
        printf("Test abort for writing failed.\r\n");
        goto RET;
    }
    cost = get_timer(start);
    printf("Write done. Cost %lds to write %dMB\r\n\r\n", cost/400, testLen>>20);

    printf("Test reading...\r\n");
    start = get_timer(0);
    rv = flash->read(flash, testOffset, testLen, buf2);
    if (rv != 0)
    {
        printf("Test abort for writing failed.\r\n");
        goto RET;
    }
    cost = get_timer(start);
    printf("Reading done. Cost %lds to read %dMB\r\n\r\n", cost/400, testLen>>20);
    
    if (memcmp(buf1, buf2, testLen) != 0)
    {
        printf("Verifing is BAD\r\n");
    }
    else
    {
        printf("Verifing is GOOD\r\n");
    }

RET:
    /* if (NULL != buf1) free(buf1); */
    /* if (NULL != buf2) free(buf2); */
    sysReboot();
    #endif
    return TRUE;
}

LOCAL bool cmdFlashReadTest(void)
{
    #if 0
    char line [BUF_LINE] = {0};
    unsigned long start = 0L;
    unsigned long cost = 0L;
    unsigned char* buf = 0x61000000;
    bool rv = TRUE;
    struct spi_flash * flash = NULL;
    int i = 0;
    int size = 0;
    int testCount = 0;

    printf("Do you really want to do this?[Y/N]:");
    cmdReadLine(line, NULL);

    if (tolower(line[0]) != 'y')
    {
        return TRUE;
    }

    flash = spi_flash_probe(0, 0, 0X1000000, 0X3);
    if (NULL == flash)
    {
        printf("Failed to attach SPI flash.\r\n");
        return TRUE;
    }
    printf("Attach flash OK. Flash info:\r\n");
    printf("Name:   %s\r\n", flash->name);
    printf("Size:   %#x\r\n", flash->size);
    printf("Sector: %#x\r\n", flash->sector_size);
    printf("Page:   %#x\r\n\r\n", flash->page_size);

    if ((flash->size < 0x100000)
        || (flash->size > 0x2000000))
    {
        printf("Invalid flash of size. Test abort.\r\n");
        return TRUE;
    }
    
    start = get_timer(0);
    srand_sftest(start);
    for (i = 0x1000000; i < 0x2000000; i = i + size)
    {
        testCount++;
        size = rand_sftest();
        size &= 0xff;
        printf("^");
        rv = flash->read(flash, i, size, buf);
        printf("$");
        /* udelay(473150); */
        if (rv != 0)
        {
            printf("Test abort for reading failed.offset=%#x, size=%#x, testCount=%d\r\n", 
                i, size, testCount);
            goto RET;
        }
    }
    cost = get_timer(start);
    printf("Reading done. Cost %lds to read %d times\r\n\r\n", cost/400, testCount);

RET:
    /* if (NULL != buf1) free(buf1); */
    /* if (NULL != buf2) free(buf2); */
    /* sysReboot(); */
    #endif
    return TRUE;
}

#endif


/**********************************************************************/
/*                            GLOBAL_FUNCTIONS                        */
/**********************************************************************/

void menu_shell(void)
{
    char readBuf[BUF_LINE];
    int status;
    char *pLine;

    cmdHelp();
#ifdef CONFIG_SYS_PROMPT
#undef CONFIG_SYS_PROMPT
#define CONFIG_SYS_PROMPT	"tplink> "  
#endif

#if 1            
    g_ExitMenuFlag = 0;
    while(!g_ExitMenuFlag)
#else
    while(1)
#endif
    {	
        /*!< read the input command (add by Cai Peifeng:2010-9-3 10:35:16)*/
        readBuf[0] = 0;
        status = cmdReadLine(readBuf, CONFIG_SYS_PROMPT);

        if( FALSE == status)
        {
            continue;
        }
        /*skip the space*/
        pLine = readBuf;
        skipSpace(&pLine);

        /*handle the command*/
        cmdParseCmd(pLine);
    }

}



int abortboot(int bootdelay)
{
	int abort = 0;


	if (bootdelay >= 0)
		printf("\nHit any key to stop autoboot: %2d ", bootdelay);


	while ((bootdelay > 0) && (!abort)) {
		int i;

		--bootdelay;
		/* delay 100 * 10ms */
		for (i=0; !abort && i<100; ++i) {
			/*在hit loop中的动态切换轮询*/
			/*extern int serialDymChangeInHitLoop(void);
			serialDymChangeInHitLoop();*/
			
			if (tstc()) {	/* we got a key press	*/
				abort  = 1;	/* don't auto boot	*/
				bootdelay = 0;	/* no more delay	*/

				(void) getc();  /* consume input	*/

				break;
			}
			udelay(10000);
		}

		printf("\b\b\b%2d ", bootdelay);
	}

	putc('\n');

	return abort;
}

void AutoStart(void)
{
    if(!abortboot(1))
    {
        cmdStart();
    }
}

int AutoFixKernel(void)
{
    int nRet = OK;
    char *buffer = NULL;
    int maxSize = FLASH_SIZE;
    int recSize = 0;
    char downFile[BUF_LINE] = {0};
    char command[CONFIG_SYS_CBSIZE];

    printf("The kernel has been damaged!\nbegin to fix kernel...\n");
    printf("Please put the uimage.img into the tftpserver directory\n");
    printf("You can press CTRL-C to stop loading\r\n");

    sprintf(downFile, "%s:uImage.img", getenv("serverip"));

    /* 获取文件 */
    if( FALSE == ftpGetImage(&buffer, downFile, &recSize, maxSize))
    {
        printf("Download failed\r\n");
        return ERROR;
    }

    if (0 == recSize)
    {
        printf("Download failed\r\n");
        return ERROR;		
    }	

    nRet = sysFixKernel(buffer, recSize);
    if(ERROR == nRet)
    {
        printf("the kernel fixing is fail\n");
        return ERROR;
    }

    printf("The kernel has been repaired, begin to startup.\n");

    memset(command, 0, sizeof(command));
    sprintf(command, "bootm 0x%x", IMAGE_ADDR);
    nRet = run_command(command, 0);
    return OK;
}


