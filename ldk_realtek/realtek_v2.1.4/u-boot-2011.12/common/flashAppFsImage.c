/******************************************************************************
*
* Copyright (c) 2010 TP-LINK Technologies CO.,LTD.
* All rights reserved.
*
* file name  :   flashAppFsImage.c
* version    :   V1.0
* description:   定义解析镜像的接口.
*
* author     :   LiuZenglin <LiuZenglin@tp-link.net>
* data       :   06/25/2013
*
* history    :
* 01   06/25/2013  LiuZenglin     Create.
*
* refresh option:
* 1、更新switch.tp 和 webImage.z
* 2、不删除原内部区的项，直接写入镜像中的内部项，如果有则覆盖，底层会处理
*    profile中mac和RSA的；

* 4、删除内部区中的除gev和profile的项，删除非必须的gev项，再写入镜像中的内部项；
*
* 记镜像中版本号为V1 A.B.C, flash中版本号为V2 A.B.C
* if ((A1 == A1) && (B1 <= B2) && (C1 <= C2))   执行操作1
* if ((A1 == A1) && (B1 <= B2) && (C1 > C2))    执行操作1和操作2
* if ((A1 == A1) && (B1 > B2))                  执行操作2和操作3
* if (A1 != A1)                                 执行操作3和操作4
*
******************************************************************************/
#include<common.h>
#include<vsprintf.h>
#include <tplink/flashApp.h>
#include <tplink/flashAppInner.h>
#include <tplink/flashAppInnerGev.h>
#include <tplink/flashAppInnerProfile.h>
#include <tplink/flashAppFsImage.h>
#include <linux/ctype.h>
#include <tplink/flashUtil.h>
#include <tplink/md5_tp.h>
#include "malloc.h"

int flash_fs_image_debug = 0;
    
#define DBG(arg...) do{if(0){printf("%s,%d:",__FUNCTION__, __LINE__);printf(arg);}}while(0)


/* 签名信息长度 */
#define RSA_SIGN_LEN	(128)
/* 用于检验升级文件签名的公钥文件 */
//static char pubKey[] = "BgIAAACkAABSU0ExAAQAAAEAAQBhVXK3wF79A6cXXFu0Y0wKz0dPQWi2dWPE7p8eY9e6PAqc5BBTQPxi2/N1OotrUN11Q6cBXA0gmflRusUiJtdARng43tSWz2pZueskCC5kH9/+1/ACi2ZY1WlK5TVu5Bh0YCzAfvlmsbuPjk/W4S3Jco+ISDOrpF5wwuxlCHI2vQ==";
/*
 * 该函数用于检查升级文件的签名是否正确
 * note:
 * 1,签名信息位于升级文件的最后128bytes
 * 2,签名信息生成规则:对整个升级文件计算md5,根据md5值计算128bytes的RSA签名信息
 * 
*/
#ifdef UBOOT_DEBUG
//extern void md5_calc(unsigned char * output,unsigned char * input, unsigned int inlen);
#if 0
static int firmwareRsaSignVerify(const unsigned char *srcFile, int *size)
{
	int imageSize = *size - RSA_SIGN_LEN;
	unsigned char md5Result[16] = {0};
	unsigned char signFile[RSA_SIGN_LEN]	= {0};
	int ret = 0;

	if (imageSize <= 0)
	{
		return ERROR;
	}
	
	md5_calc(md5Result, (unsigned char *)srcFile, imageSize);
	
	memcpy(signFile, srcFile+imageSize, RSA_SIGN_LEN);
	
	//ret = rsaVerifySignByBase64EncodePublicKeyBlob(pubKey, sizeof(pubKey)-1, md5Result, 16, signFile, RSA_SIGN_LEN);
	ret = 1;
	if (ret)
	{
		/* T3700不移除镜像中的签名信息 */
		*size = imageSize;	
		printf("firmware rsa verify pass.\r\n");
		return OK;
	}
	else
	{
		printf("Failed to verify Rsa.\r\n");
		return ERROR;
	}
}

#endif
#if 0
/* refresh option 1 */
LOCAL int _refreshOverwriteFs(const IMAGE_INFO *imageInfo, const char* pBuff)
{
    printf("Start to init the configuration of the image...\r\n");
    if (FALSE == imageInfo->isValid)
    {
        printf("Fatal error: The start image is valid\r\n");
        return ERROR;
    }

    DBG("Info: Write switch.tp for starting...\r\n");
    if (FALSE == bootWriteFile(TP_SWITCH_FILE, (const char *)(pBuff + imageInfo->switch_tp_offset),
        imageInfo->switch_tp_len, FALSE))
    {
        printf("Fatal error: Failed to init configuration\r\n");
        return FALSE;
    }

    DBG("Info: Write webImage.z for starting...\r\n");
    if (FALSE == bootWriteFile(TP_WEBIMAGE_FILE, (const char *)(pBuff + imageInfo->webImage_offset),
        imageInfo->webImage_len, FALSE))
    {
        printf("Fatal error: Failed to init web image\r\n");
        return FALSE;
    }

    printf("Init the configuration of the image done\r\n");

    return TRUE;
}

typedef struct _FS_FILE_NODE
{
    unsigned char* pBuf;
    int  length;        /* 文件的长度 */
    char filename[FN_MAX_LENGTH];
    struct _FS_FILE_NODE *next;
}FS_FILE_NODE;

LOCAL FS_FILE_NODE *fsFileHead = NULL;
/* 释放内存 */
LOCAL void _freeFsFileList(void)
{
    FS_FILE_NODE *pCur = fsFileHead;
    FS_FILE_NODE *pNext = NULL;

    while (pCur != NULL)
    {
        pNext = pCur->next;

        if (pCur->pBuf != NULL)
        {
            free(pCur->pBuf);
        }

        free(pCur);
        pCur = pNext;
        pNext = NULL;
    }
    return;
}

/* refresh option 3 */
LOCAL int _refreshInitFs(const IMAGE_INFO *imageInfo, const char* pBuff)
{
    DIR *root = NULL;
    struct dirent *temp = NULL;
    char fileName[FN_MAX_LENGTH] = {0};
    int fnLen = 0;
    int fileNo = 0;
    FS_FILE_NODE *pCur = NULL;
    FS_FILE_NODE *pParent = NULL;
    FS_FILE_NODE *pTemp = NULL;
    FS_FILE_NODE *pTail = NULL;

    printf("Start to init the file system...\r\n");
    printf("Start to backup all files in the file system...\r\n");
    /* 读取所有文件 */
    DBG("Get all files in the filesystem.\r\n");
    root = opendir(FLASH_FS_NAME);
    if (NULL == root)
    {
        printf("Fatal error: Failed to open fs\r\n");
        return ERROR;
    }

    pParent = fsFileHead;
    pCur = NULL;
    while ((NULL != (temp = readdir(root))) && (fileNo < SYSTEM_MAX_FILE_NUMBER))
    {
        fnLen = strlen(temp->d_name);
        if ((fnLen > 0) && (fnLen < SYSTEM_MAXLEN_FILE_NAME))
        {
            fileNo++;
            pCur = (FS_FILE_NODE*)calloc(1, sizeof(FS_FILE_NODE));
            if (pCur == NULL)
            {
                printf("Fatal error: Failed to allocate memory, size=%#x.\r\n", sizeof(FS_FILE_NODE));
                _freeFsFileList();
                return ERROR;
            }
            pCur->next = NULL;

            sprintf(fileName, "%s%s", FLASH_FS_NAME, temp->d_name);
            strncpy(pCur->filename, fileName, FN_MAX_LENGTH);    /* 保存文件名 */
             /* 如果为switch.tp和webImage，在最后面添加到链表头部 */
            if ((strncmp(temp->d_name, "flash:switch.tp", FN_MAX_LENGTH) != 0) &&
                (strncmp(temp->d_name, "flash:webImage.z", FN_MAX_LENGTH) != 0))
            {
                if (FALSE == bootReadFile(fileName, &(pCur->pBuf), &(pCur->length)))
                {
                    DBG("Failed to read file, fn=%s.\r\n", fileName);
                    free(pCur);
                    pCur = NULL;
                    continue;
                }
            }
            else    /* 遍历过程中不读取switch.tp和webImage.z */
            {
                free(pCur);
                pCur = NULL;
                continue;
            }

            if (pCur->pBuf != NULL)
            {
                printf(".");
            }
        }
        else
        {
            DBG("Bad file length, length=%#x.\r\n", fnLen);
        }

        /* 连接成链表 */
        if (fsFileHead == NULL)
        {
            fsFileHead = pCur; 
            pParent = pCur;
            pCur = NULL;           
        }
        else
        {
            /* 如果文件名后缀为*.bin或*.cfg则写入到头部处  */
            if ((fnLen > 4) &&
                ((strncmp((char*)(pCur->filename) + fnLen - 4, ".bin", 10) == 0) ||
                (strncmp((char*)(pCur->filename) + fnLen - 4, ".cfg", 10) == 0)))
            {
                pTail = pParent;
                pParent = fsFileHead;   /* 此处head肯定不为空 */             

                fsFileHead = pCur;
                pCur->next = pParent->next;
                pParent = pTail;
                pCur = NULL;
            }
            else    /* 不特殊处理直接连到列表最后面 */
            {
                pParent->next = pCur;
                pParent = pCur;
                pCur = NULL;
            }
        }
    }
    closedir(root);

    printf(".");
    /* 把switch.tp和webImage写入到头部 */
    pCur = (FS_FILE_NODE*)calloc(1, sizeof(FS_FILE_NODE));
    if (pCur == NULL)
    {
        printf("Fatal error: Failed to allocate memory, size=%#x.\r\n", sizeof(FS_FILE_NODE));
        _freeFsFileList();
        return ERROR;
    }
    pCur->next = NULL;
    pCur->pBuf = (unsigned char*)calloc(1, imageInfo->switch_tp_len);
    if (pCur->pBuf == NULL)
    {
        printf("Fatal error: Failed to allocate memory, size=%#x.\r\n", imageInfo->switch_tp_len);
        _freeFsFileList();
        return ERROR;
    }
    memcpy(pCur->pBuf, (unsigned char*)(pBuff+imageInfo->switch_tp_offset), imageInfo->switch_tp_len);
    pCur->length = imageInfo->switch_tp_len;
    strncpy(pCur->filename, "flash:switch.tp", FN_MAX_LENGTH);

    if (fsFileHead == NULL)
    {
        fsFileHead = pCur;
    }
    else
    {
        pTemp = fsFileHead;
        fsFileHead = pCur;
        fsFileHead->next = pTemp;
        pCur = NULL;
    }

    printf(".");
    pCur = (FS_FILE_NODE*)calloc(1, sizeof(FS_FILE_NODE));
    if (pCur == NULL)
    {
        printf("Fatal error: Failed to allocate memory, size=%#x.\r\n", sizeof(FS_FILE_NODE));
        _freeFsFileList();
        return ERROR;
    }
    pCur->next = NULL;
    pCur->pBuf = (unsigned char*)calloc(1, imageInfo->webImage_len);
    if (pCur->pBuf == NULL)
    {
        printf("Fatal error: Failed to allocate memory, size=%#x.\r\n", imageInfo->webImage_len);
        _freeFsFileList();
        return ERROR;
    }
    memcpy(pCur->pBuf, (unsigned char*)(pBuff+imageInfo->webImage_offset), imageInfo->webImage_len);
    pCur->length = imageInfo->webImage_len;
    strncpy(pCur->filename, "flash:webImage.z", FN_MAX_LENGTH);

    if (fsFileHead == NULL)
    {
        fsFileHead = pCur;
    }
    else
    {
        pTemp = fsFileHead;
        fsFileHead = pCur;
        fsFileHead->next = pTemp;
        pCur = NULL;
    }
    printf("\r\n");
    DBG("Get all files in the filesystem done.\r\n");
    /* 读取完毕 */
    printf("Backup files in the file system done\r\n");

    /* 调试信息 */
    if (flash_fs_image_debug)
    {
        DBG("File in list: \r\n");
        pTemp = fsFileHead;
        while (pTemp != NULL)
        {
            DBG("filename=%s, length=%d\r\n",
                pTemp->filename, pTemp->length);
            pTemp = pTemp->next;
        }
    }
    /* end调试信息 */

    printf("Start to format the file system...\r\n");
    DBG("Format file system.\r\n");
    /* 格式化文件系统 */
    if (ERROR == flTffsFormatVol())
    {
        printf("Fatal error: Failed to format fs.\r\n");
        _freeFsFileList();
        return ERROR;
    }
    
    printf("\r\n");
    DBG("Format file system done.\r\n");
    /* 格式完毕 */
    printf("Format the file system done\r\n");

    printf("Start to restore the files to file system...\r\n");
    /* 写入文件 */
    DBG("Restore all files to the filesystem.\r\n");
    pCur = fsFileHead;
    while (pCur != NULL)
    {
        /* 如果为空则说明为空文件 */
        if (pCur->pBuf != NULL)
        {
            printf(".");
            if (FALSE == bootWriteFile(pCur->filename, pCur->pBuf, pCur->length, FALSE))
            {
                DBG("Failed to write file, fn=%s.\r\n", fileName);
            }
        }

        pCur = pCur->next;
    }
    printf("\r\n");
    DBG("Restore all files done.\r\n");
    /* 写入完成 */
    printf("Restore the files to file system done\r\n");

    /* 释放内存 */
    _freeFsFileList();

    return TRUE;
}
#endif

extern int sysGetFlAddr(char * key);
extern int sysGetMtdpartsInfo(char * key);
extern UINT32 l_writeFlash_noErase(char *source,unsigned int size, unsigned int flashOffset,int prompt);



/*如果没有内核则只更新usrImg, 如果有内核，则内核也需要更新
* 更新原则是，如果布局发生变化，那么内核+用户镜像+用户文件系统+内部区都要更新
* 效率比较低，一般情况下不要更新布局--caodahong
*/
LOCAL int _refreshImages(const IMAGE_INFO *imageInfo, const char* pBuff, int nRegion)
{   
    int kerneloffset = 0;
    int kernelsize = 0;
    int usrimgoffset = 0;
    int usrimgsize = 0;
    int ubootsize = 0;
    int ubootoffset = 0;
    char * pReadBufusrImg = NULL;
    char * pReadBufusrApp = NULL;
    int usrImg1NeedMove = 0;
    int usrImg2NeedMove = 0;
    int usrAppNeedMove = 0;
    //int nMemOffset = 0;
 
    if ((imageInfo == NULL) ||(pBuff == NULL))
    {
        DBG("Fatal Error: Bad parameter, imageInfo=%#x, pBuff=%#x\r\n",(int)imageInfo, (int)pBuff);
        return ERROR;
    }

    CHECKREGION(nRegion);

    kerneloffset = imageInfo->uImage_offset;
    kernelsize = imageInfo->uImage_len;
    usrimgoffset = imageInfo->usrImage_offset;
    usrimgsize = imageInfo->usrImage_len;
    ubootsize = imageInfo->bootrom_len;
    ubootoffset = imageInfo->bootrom_offset;

    //各个镜像在FLASH中的偏移和大小
    int flKernelSize = simple_strtoul(imageInfo->flKernelSize, NULL, 16);
    //int flKernelOffset = simple_strtoul(imageInfo->flKernelOffset, NULL, 16);
    int flUsrImg1Size = simple_strtoul(imageInfo->flUsrImg1Size, NULL, 16);
    int flUsrImg1Offset = simple_strtoul(imageInfo->flUsrImg1Offset, NULL, 16);
    int flUsrImg2Size = simple_strtoul(imageInfo->flUsrImg2Size, NULL, 16);
    int flUsrImg2Offset = simple_strtoul(imageInfo->flUsrImg2Offset, NULL, 16);
    int flUsrAppSize = simple_strtoul(imageInfo->flUsrAppSize, NULL, 16);
    int flUsrAppOffset = simple_strtoul(imageInfo->flUsrAppOffset, NULL, 16);

    int oldKernelOffset = sysGetFlAddr("sys");
    int oldKernelSize = sysGetMtdpartsInfo("sys");
    int oldUsrImg1Offset = sysGetFlAddr("usrimg1");
    int oldUsrImg1Size = sysGetMtdpartsInfo("usrimg1");
    int oldUsrImg2Offset = sysGetFlAddr("usrimg2");
    int oldUsrImg2Size = sysGetMtdpartsInfo("usrimg2");
    int oldUsrAppOffset = sysGetFlAddr("usrapp");
    int oldUsrAppSize = sysGetMtdpartsInfo("usrapp");

    if((oldUsrAppOffset + oldUsrAppSize) > (FLASH_SIZE -0x40000)) /*要保证内部区不能被覆盖*/
    {
        DBG("reflash: the old mtd parts is error: 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n",
                    oldKernelOffset, oldKernelSize, oldUsrImg1Offset, 
                    oldUsrImg1Size, oldUsrImg2Offset, 
                    oldUsrImg2Size,oldUsrAppOffset,oldUsrAppSize);
    }

    DBG("[%x %x %x %x]<---->[%x %x %x %x]\n", flKernelSize, flUsrImg1Size,flUsrImg2Size, flUsrAppSize, oldKernelSize , oldUsrImg1Size, oldUsrImg2Size,oldUsrAppSize);

    if(flKernelSize != oldKernelSize)
    {
        usrImg1NeedMove = 1;
        usrImg2NeedMove = 1;
        usrAppNeedMove = 1;
    }
    else if(flUsrImg1Size != oldUsrImg1Size)
    {
        usrImg2NeedMove = 1;
        usrAppNeedMove = 1;
    }
    else if(flUsrImg2Size != oldUsrImg2Size)
    {
        usrAppNeedMove = 1;
    }
    
    if(1 == nRegion)
    {
        usrImg1NeedMove = 0;/*要升级的区域不需要移动*/
    }
    else if(2 == nRegion)
    {
        usrImg2NeedMove = 0;/*要升级的区域不需要移动*/
    }
    else
    {
        return ERROR;
    }

   
    /*如果usrapp的地址变大了，那么先刷usrapp, 后刷usrimg, 否则反过来*/
    if((1 == usrAppNeedMove) && (flUsrAppOffset > oldUsrAppOffset) )
    {
        pReadBufusrApp = (char *)BUF_ADDR_NOFREE;
        if(NULL == pReadBufusrApp)
        {
            return ERROR;
        }
        memset(pReadBufusrApp, 0, oldUsrAppSize);
        DBG("reflash usrapp: (memaddr: 0x%x) 0x%x 0x%x 0x%x \n",(int)pReadBufusrApp,  oldUsrAppSize, oldUsrAppOffset, flUsrAppOffset);
        l_readFlash(pReadBufusrApp, oldUsrAppSize, oldUsrAppOffset);

        flashErase(flUsrAppOffset/FLASH_SECTOR_SIZE, flUsrAppSize/FLASH_SECTOR_SIZE);
        l_writeFlash_noErase(pReadBufusrApp, flUsrAppSize, flUsrAppOffset, true);
    }

    if(1 == usrImg1NeedMove)
    {
        pReadBufusrImg = (char *)BUF_ADDR_NOFREE;
        if(NULL == pReadBufusrImg)
        {
            return ERROR;
        }
        memset(pReadBufusrImg, 0, oldUsrImg1Size);
        DBG("reflash usrImg1: 0x%x 0x%x 0x%x \n", oldUsrImg1Size, oldUsrImg1Offset, flUsrImg1Offset);
        l_readFlash(pReadBufusrImg, oldUsrImg1Size, oldUsrImg1Offset);
        flashErase(flUsrImg1Offset/FLASH_SECTOR_SIZE, flUsrImg1Size/FLASH_SECTOR_SIZE);
        l_writeFlash_noErase(pReadBufusrImg, flUsrImg1Size, flUsrImg1Offset, true);

    }

    if(1 == usrImg2NeedMove)/*1区和2区只有一个区可能被移动*/
    {
        pReadBufusrImg = (char *)BUF_ADDR_NOFREE;
        if(NULL == pReadBufusrImg)
        {
            return ERROR;
        }
        memset(pReadBufusrImg, 0, oldUsrImg1Size);
        DBG("reflash usrImg2: 0x%x 0x%x 0x%x \n", oldUsrImg2Size, oldUsrImg2Offset, flUsrImg2Offset);
        l_readFlash(pReadBufusrImg, oldUsrImg2Size, oldUsrImg2Offset);
        flashErase(flUsrImg2Offset/FLASH_SECTOR_SIZE, flUsrImg2Size/FLASH_SECTOR_SIZE);
        l_writeFlash_noErase(pReadBufusrImg, flUsrImg2Size, flUsrImg2Offset, true);     

    }
    /*如果usrapp的地址变小了，那么先刷usrimg, 后刷usrapp*/    
    if((1 == usrAppNeedMove) && (flUsrAppOffset < oldUsrAppOffset) )
    {
        pReadBufusrApp = (char *)BUF_ADDR_NOFREE;
        if(NULL == pReadBufusrApp)
        {
            return ERROR;
        }
        memset(pReadBufusrApp, 0, oldUsrAppSize);
        DBG("reflash usrapp: (memaddr: 0x%x) 0x%x 0x%x 0x%x \n",(unsigned int)pReadBufusrApp,  oldUsrAppSize, oldUsrAppOffset, flUsrAppOffset);
        l_readFlash(pReadBufusrApp, oldUsrAppSize, oldUsrAppOffset);

        flashErase(flUsrAppOffset/FLASH_SECTOR_SIZE, flUsrAppSize/FLASH_SECTOR_SIZE);
        l_writeFlash_noErase(pReadBufusrApp, flUsrAppSize, flUsrAppOffset, true);
    }

    return OK;
    
}

/* refresh option 2 */
LOCAL int _refreshOverwriteInnerItems(const IMAGE_INFO *imageInfo, const char* pBuff)
{
    INNER_ITEM_INFO_NODE *innerItemInfoNode = imageInfo->innerItemHead;

    DBG("Start to init the flash for the image...\r\n");

    DBG("Write flash distribution info to gev, bootsize=%s, fsOffset=%s, fsSize=%s, flVer=%s.\r\n",
        imageInfo->flBootSize, imageInfo->flKernelOffset, imageInfo->flKernelSize, imageInfo->flVer);
    /* 先写入环境变量 */
    if (ERROR == setEnvItem(TP_SWITCH_FL_SIZE_BOOT, imageInfo->flBootSize))
    {
        DBG("Fatal error: Failed to set gev, %s=%s\r\n", TP_SWITCH_FL_SIZE_BOOT, imageInfo->flBootSize);
        return ERROR;
    }
    if (ERROR == setEnvItem(TP_SWITCH_FL_OFFSET_KERNEL, imageInfo->flKernelOffset))
    {
        DBG("Fatal error: Failed to set gev, %s=%s\r\n", TP_SWITCH_FL_OFFSET_KERNEL, imageInfo->flKernelOffset);
        return ERROR;
    }
    if (ERROR == setEnvItem(TP_SWITCH_FL_SIZE_KERNEL, imageInfo->flKernelSize))
    {
        DBG("Fatal error: Failed to set gev , %s=%s\r\n", TP_SWITCH_FL_SIZE_KERNEL, imageInfo->flKernelSize);
        return ERROR;
    }
    if (ERROR == setEnvItem(TP_SWITCH_FL_OFFSET_USRIMG1, imageInfo->flUsrImg1Offset))
    {
        DBG("Fatal error: Failed to set gev, %s=%s\r\n", TP_SWITCH_FL_OFFSET_USRIMG1, imageInfo->flUsrImg1Offset);
        return ERROR;
    }
    if (ERROR == setEnvItem(TP_SWITCH_FL_SIZE_USRIMG1, imageInfo->flUsrImg1Size))
    {
        DBG("Fatal error: Failed to set gev , %s=%s\r\n", TP_SWITCH_FL_SIZE_USRIMG1, imageInfo->flUsrImg1Size);
        return ERROR;
    }
    if (ERROR == setEnvItem(TP_SWITCH_FL_OFFSET_USRIMG2, imageInfo->flUsrImg2Offset))
    {
        DBG("Fatal error: Failed to set gev, %s=%s\r\n", TP_SWITCH_FL_OFFSET_USRIMG2, imageInfo->flUsrImg2Offset);
        return ERROR;
    }
    if (ERROR == setEnvItem(TP_SWITCH_FL_SIZE_USRIMG2, imageInfo->flUsrImg2Size))
    {
        DBG("Fatal error: Failed to set gev , %s=%s\r\n", TP_SWITCH_FL_SIZE_USRIMG2, imageInfo->flUsrImg2Size);
        return ERROR;
    }
    
    if (ERROR == setEnvItem(TP_SWITCH_FL_OFFSET_USRAPP, imageInfo->flUsrAppOffset))
    {
        DBG("Fatal error: Failed to set gev, %s=%s\r\n", TP_SWITCH_FL_OFFSET_USRAPP, imageInfo->flUsrAppOffset);
        return ERROR;
    }
    if (ERROR == setEnvItem(TP_SWITCH_FL_SIZE_USRAPP, imageInfo->flUsrAppSize))
    {
        DBG("Fatal error: Failed to set gev , %s=%s\r\n", TP_SWITCH_FL_SIZE_USRAPP, imageInfo->flUsrAppSize);
        return ERROR;
    }
    if (ERROR == setEnvItem(TP_SWITCH_FL_VER, imageInfo->flVer))
    {
        DBG("Fatal error: Failed to set gev, %s=%s\r\n", TP_SWITCH_FL_VER, imageInfo->flVer);
        return ERROR;
    }

    /* 遍历写入镜像中所有内部项 */
    while (innerItemInfoNode != NULL)
    {
        DBG("Write inner item: type=%d, offset=%#x, length=%#x\r\n", 
            innerItemInfoNode->type, innerItemInfoNode->offset, innerItemInfoNode->length);
        if (ERROR == flashAppInnerSet(innerItemInfoNode->type, (char*)(pBuff+innerItemInfoNode->offset), 
            innerItemInfoNode->length, FALSE))
        {
            DBG("Fatal error: Failed to set innner item, type=%#x, offset=%#x, length=%#x\r\n", 
                innerItemInfoNode->type, innerItemInfoNode->offset, innerItemInfoNode->length);
            return ERROR;
        }
        innerItemInfoNode = innerItemInfoNode->next;
    }

    DBG("Init the flash for the image done\r\n");

    return OK;
}



/* refresh option 4 */
/* 删除内部区中除profile和GEV外的其它内部项 */


LOCAL int _refreshInitInnerItems(const IMAGE_INFO *imageInfo, const char* pBuff)
{
    /*printf("Start to reinit the flash...\r\n");
    if (ERROR == flashAppInnerReset(imageInfo->flBootSize, imageInfo->flFsOffset, imageInfo->flFsSize, imageInfo->flVer))
    {
        printf("Fatal error: Failed to init flash.\r\n");
        return ERROR;
    }
    printf("Reinit the flash done\r\n");*/

    return OK;
}



/* 启动镜像时，刷新FLASH，记版本号为V A.B.C */

int refreshFlash(const IMAGE_INFO *imageInfo, const char* pBuff,  int nRegion)
{


    int flVerInImg = 0;
    char flVerInImgA = 0;
    char flVerInImgB = 0;
    char flVerInImgC = 0;
    int flVerInFlash = 0;
    char flVerInFlashA = 0;
    char flVerInFlashB = 0;
    char flVerInFlashC = 0;
    char gevValue[ENV_ITEM_VALUE_LEN] = {0};

    if ((imageInfo == NULL) ||
        (pBuff == NULL))
    {
        DBG("Fatal Error: Bad parameter, imageInfo=%#x, pBuff=%#x\r\n",
            (int)imageInfo, (int)pBuff);
        return ERROR;
    }

    CHECKREGION(nRegion);

    if (FALSE == imageInfo->isValid)
    {
        DBG("Fatal error: The start image is valid\r\n");
        return ERROR;
    }
    
    flVerInImg = strtoul(imageInfo->flVer, NULL, 16);
    if (ERROR == getEnvItem(TP_SWITCH_FL_VER, gevValue))
    {
        DBG("Fatal error: Failed to get flash ver in flash.\r\n");
        return ERROR;
    }

    flVerInFlash = strtoul(gevValue, NULL, 16);

    flVerInImgA = (flVerInImg & 0xff0000) >> 16;
    flVerInImgB = (flVerInImg & 0xff00) >> 8;
    flVerInImgC = (flVerInImg & 0xff);
    flVerInFlashA = (flVerInFlash & 0xff0000) >> 16;
    flVerInFlashB = (flVerInFlash & 0xff00) >> 8;
    flVerInFlashC = (flVerInFlash & 0xff);
    /*printf("flVerInImg=V%d.%d.%d, flVerInFlash=V%d.%d.%d\r\n", 
       flVerInImgA, flVerInImgB, flVerInImgC, flVerInFlashA, flVerInFlashB, flVerInFlashC);*/

    /* 系统号相同 */
    if (flVerInImgA == flVerInFlashA)
    {
        /* flash中布局更高，不重新初始化文件系统 */
        if (flVerInImgB <= flVerInFlashB)
        {
            /* 镜像中索引号高，需要写入新内部区条目，否则不需要 */
            if (flVerInImgC > flVerInFlashC)
            {                
                /* 覆盖内部区 */
                if (ERROR == _refreshOverwriteInnerItems(imageInfo, pBuff))
                {
                    DBG("Fatal error: Failed to refresh overwrite inner items.\r\n");
                    goto ERR;
                }                
            }
        }
        /* 需要更新FLASH布局，需要重新格式化文件系统 */
        else
        {  

            
            if(ERROR == _refreshImages(imageInfo, pBuff, nRegion))
            {
                DBG("Fatal error: faile to reflash images.\r\n");
                goto ERR;
            }

             /* 覆盖内部区 */
            if (ERROR == _refreshOverwriteInnerItems(imageInfo, pBuff))
            {
                DBG("Fatal error: Failed to refresh overwrite inner items.\r\n");
                goto ERR;
            }
            
            /* 需要设置环境变量之后，重新获取FLASH布局参数 */
            if (ERROR == flashAppInit())
            {
                DBG("Fatal error: Failed to init flash app.\r\n");
                goto ERR;                
            }
        


        }
    }
    /* 系统号不同 */
    else
    {

        if(ERROR == _refreshImages(imageInfo, pBuff, nRegion))
        {
            DBG("Fatal error: faile to reflash images.\r\n");
            goto ERR;
        }
                    
        if (ERROR == _refreshInitInnerItems(imageInfo, pBuff))
        {
            DBG("Fatal error: Failed to refresh init inner item area.\r\n");
            goto ERR;
        }

    }

    return OK;
ERR:
    DBG("Fatal error: Failed to init Flash.\r\n");
    return ERROR;
}

  
extern void free(void*);
/* 释放镜像中原有的内部项信息 */
LOCAL int _freeImageInfoInnerItem(INNER_ITEM_INFO_NODE *pHead)
{
    INNER_ITEM_INFO_NODE *pNext = NULL;
    INNER_ITEM_INFO_NODE *pFree = pHead;

    while (NULL != pFree)
    {
        pNext = pFree->next;
        free(pFree);
        pFree = pNext;
        pNext = NULL;
    }

    return OK;
}

extern int sysDesDecode(unsigned char *in_buf,int in_len,unsigned char *out_buf);

/* 解析传入的镜像buff，buff长度为length */
/* 输出镜像信息pImageInfo */
/* 如果传入的buff为空，或者内容不是有效镜像，或者productId不正确则返回ERROR */
/* 解析正确，则返回OK */
int sysParseImage(const unsigned char* pImageBuff, int length, IMAGE_INFO *pImageInfo)
{
    int innerItemOffset = 0;        /* 内部项的type字段偏移量 */
    char imageName[BOOT_FILE_LEN] = {0};
    //int imageSize = 0;
    TP_IMAGE_HEADER header = {{0}};
    INNER_ITEM_INFO_NODE *pCur = NULL;      /* 指示当前节点 */
    INNER_ITEM_INFO_NODE *pNewNode = NULL;
    #if 1
    //unsigned char *pMd5Key = NULL;
    //unsigned char    *pUndesBuf = NULL;
    char    fileMd5Checksum[MD5_DIGEST_LEN];
    char    calcMd5Checksum[MD5_DIGEST_LEN];
    #endif
    int i = 0;
    int rc = 0;

    if ((NULL == pImageBuff) || (length <= FIRMWARE_MIN_SIZE) ||
        (length >= FIRMWARE_MAX_SIZE) || (pImageInfo == NULL))
    {
        DBG("Fatal error: Bad parameter in parsing image, pImageBuffer=%#x, length=%#x, pImageInfo=%#x.\r\n",
            (int)pImageBuff, length, (int)pImageInfo);
        return ERROR;
    }
	
    /*pUndesBuf = (char*)calloc(1, length-RSA_SIGN_LEN);
    if (NULL == pUndesBuf)
    {
        printf("Fatal error: Failed to allocate memory, size=%#x\r\n", length);
        return ERROR;
    }*/
#ifdef UBOOT_DEBUG
#else
    if (firmwareRsaSignVerify(pImageBuff, &length) == ERROR)
    {
        rc = ERROR;
        goto RET;
    }	
#endif

    /* 解析文件之前先释放镜像信息中的内部项 */
    if (ERROR == _freeImageInfoInnerItem(pImageInfo->innerItemHead))
    {
        printf("Fatal error: Failed to free inner item in image info.\r\n");
        //free(pUndesBuf);
        return ERROR;
    }

    /* 保留镜像名称 */
    memcpy(imageName, pImageInfo->imageName, BOOT_FILE_LEN);
    memset(pImageInfo, 0, sizeof(IMAGE_INFO));
    memcpy(pImageInfo->imageName, imageName, BOOT_FILE_LEN);
#if 1
    printf("Info: wait a moment...\r\n");
    //sysDesDecode((unsigned char*)pImageBuff, length-RSA_SIGN_LEN, pUndesBuf);
    sysDesDecode((unsigned char*)pImageBuff, length, (unsigned char *)BUF_IMAGE_ADDR);

    memcpy((unsigned char*)pImageBuff, (char *)BUF_IMAGE_ADDR, length);

    memcpy(&header, (char *)BUF_IMAGE_ADDR, sizeof(TP_IMAGE_HEADER));

    header.content.usrImageSize = ntohl(header.content.usrImageSize);
    header.content.uImageSize = ntohl(header.content.uImageSize);
    header.content.bootromSize = ntohl(header.content.bootromSize);
    header.content.compressMethod = ntohl(header.content.compressMethod);

    /* save the checksum comes with downloaded file*/
    memcpy(fileMd5Checksum, header.content.md5, MD5_DIGEST_LEN);

    memset((char*)(BUF_IMAGE_ADDR + MD5_CHECKSUM_OFFSET), 0, MD5_DIGEST_LEN);

    /* caculate the MD5 digest*/
    md5_calc((unsigned char *)calcMd5Checksum, (unsigned char *)BUF_IMAGE_ADDR, length);

    DBG("Info: Check MD5 of image...\r\n");

    /* compare the checksum*/
    if (memcmp(fileMd5Checksum, calcMd5Checksum, MD5_DIGEST_LEN) != 0)
    {
        DBG("Error: MD5 checksum error\r\n");
        DBG("Error: Failed to parse image\r\n");
        for (i = 0; i < 16; i++)
        {
            DBG("0x%02x ", fileMd5Checksum[i] & 0xff);
        }
        printf("\r\n");
        for (i = 0; i < 16; i++)
        {
            DBG("0x%02x ", calcMd5Checksum[i] & 0xff);
        }
        DBG("\r\n");
        rc = ERROR;
        goto RET;
    }

    DBG("Info: Check MD5 of image OK!\n");


    DBG("Info: Check productID of image...\r\n");
#endif
    /* 检查productID */
    if (OK != sysFirmwareCheckProduct(header.content.productId))
    {
        printf("Error: Verifing productID error, image name is %s.\r\n", header.content.imagename);
        DBG("Image productID:\r\n");
        for (i = 0; i < 16; i++)
        {
            DBG("0x%02x ", (header.content.productId[i] & 0xff));
        }
        rc = ERROR;
        goto RET;
    }

    DBG("Info: Check productID of image ok\r\n");

    if (header.content.usrImageSize == 0)
    {
        rc = ERROR;
        goto RET;
    }
    pImageInfo->isValid = TRUE;
    strncpy(pImageInfo->imagename, header.content.imagename, TP_IMAGE_NAME_SIZE);

    pImageInfo->usrImage_offset = sizeof(TP_IMAGE_HEADER);
    pImageInfo->usrImage_len = header.content.usrImageSize;
    pImageInfo->uImage_offset = pImageInfo->usrImage_offset + ROUNDUP(pImageInfo->usrImage_len, 4);
    pImageInfo->uImage_len = header.content.uImageSize;
    pImageInfo->bootrom_offset = pImageInfo->uImage_offset + ROUNDUP(pImageInfo->uImage_len, 4);
    pImageInfo->bootrom_len = header.content.bootromSize;
    /* 初始化内部区 */
    innerItemOffset = (pImageInfo->bootrom_offset)+(ROUNDUP(pImageInfo->bootrom_len,4));

    /* 旧版本镜像中不包括内部区 */
    if (length == ROUNDUP(innerItemOffset, 16))
    {
        DBG("Old image, we don't read inner items.\r\n");
        pImageInfo->innerItemHead = NULL;
        memset(pImageInfo->flVer, 0, FL_VER_LEN);
        goto OLD_IMAGE;
    }
    else if (length < ROUNDUP(innerItemOffset, 16))
    {
        DBG("Fatal error: Bad image, length=%#x, inneritemoffset=%#x\r\n",
            length, ROUNDUP(innerItemOffset, 16));
        rc = ERROR;
        goto RET;
    }
    else
    {
        DBG("New image, we will read inner items.\r\n");
    }

    pImageInfo->innerItemNumber = *(int*)(BUF_IMAGE_ADDR+innerItemOffset);
    pImageInfo->innerItemNumber = ntohl(pImageInfo->innerItemNumber);

    innerItemOffset += 4;   /* innerItemNumber的长度为4 */
    i = pImageInfo->innerItemNumber;    /* 不包括gev，所以对V1.0.0只有一个内部项 */
    DBG("There is(are) %d inner item(s) in the image.\r\n", i);

    while (i != 0)
    {
        pNewNode = (INNER_ITEM_INFO_NODE*)calloc(1, sizeof(INNER_ITEM_INFO_NODE));
        if (pNewNode == NULL)
        {
            DBG("Fatal error: Failed to allocate memory for inner item, size=%#x.\r\n",
                sizeof(INNER_ITEM_INFO_NODE));
            rc = ERROR;
            goto RET;
        }
        pNewNode->type = *(int*)(BUF_IMAGE_ADDR+innerItemOffset);
        pNewNode->type = ntohl(pNewNode->type);
        innerItemOffset += 4;   /* type的长度为4 */
        
        pNewNode->length = *(int*)(BUF_IMAGE_ADDR+innerItemOffset);
        pNewNode->length = ntohl(pNewNode->length);
        innerItemOffset += 4;   /* length的长度为4 */

        /* 记录内部项的偏移地址 */
        pNewNode->offset = innerItemOffset;
        DBG("Inner item in image, type=%d, length=%#x, offset=%#x\r\n",
            pNewNode->type, pNewNode->length, pNewNode->offset);

        /* 下一个条目的偏移地址 */
        innerItemOffset += (ROUNDUP(pNewNode->length,4));

        pNewNode->next = NULL;
        /* 连接到镜像信息中 */
        if (i == pImageInfo->innerItemNumber)   /* 第一个 */
        {
            pImageInfo->innerItemHead = pNewNode;
            pCur = pNewNode;
            pNewNode = NULL;
        }
        else
        {
            pCur->next = pNewNode;
            pCur = pNewNode;
            pNewNode = NULL;
        }
        i--;
    }


    /* 初始化环境变量区 */
    pImageInfo->gevType = 1;
    memcpy(pImageInfo->flBootSize, header.content.flBootSize, FL_SIZE_LEN);
    memcpy(pImageInfo->flKernelOffset, header.content.flKernelOffset, FL_SIZE_LEN);
    memcpy(pImageInfo->flKernelSize, header.content.flKernelSize, FL_SIZE_LEN);
    memcpy(pImageInfo->flUsrImg1Offset, header.content.flUsrImg1Offset, FL_SIZE_LEN);
    memcpy(pImageInfo->flUsrImg1Size, header.content.flUsrImg1Size, FL_SIZE_LEN);
    memcpy(pImageInfo->flUsrImg2Offset, header.content.flUsrImg2Offset, FL_SIZE_LEN);
    memcpy(pImageInfo->flUsrImg2Size, header.content.flUsrImg2Size, FL_SIZE_LEN);
    memcpy(pImageInfo->flUsrAppOffset, header.content.flUsrAppOffset, FL_SIZE_LEN);
    memcpy(pImageInfo->flUsrAppSize, header.content.flUsrAppSize, FL_SIZE_LEN);
    memcpy(pImageInfo->flVer, header.content.flVer, FL_VER_LEN);

OLD_IMAGE:
    /*
    if (strlen(pImageInfo->flVer) == 0)
    {
        strcpy(pImageInfo->flVer, "0x10000");
        strcpy(pImageInfo->flBootSize, "0x100000");
        strcpy(pImageInfo->flKernelOffset, "0x100000");
        strcpy(pImageInfo->flKernelSize, "0x700000");
        strcpy(pImageInfo->flUsrImgOffset, "0xf00000");
        strcpy(pImageInfo->flUsrImgSize, "0x600000");
        strcpy(pImageInfo->flUsrAppOffset, "0x1b00000");
        strcpy(pImageInfo->flUsrAppSize, "0x200000");
    }*/

    DBG("Gev in image, BootSize=0x%x, KernelSize=0x%x, UsrImgSize=0x%x\r\n",
        pImageInfo->bootrom_len, pImageInfo->uImage_len, 
        pImageInfo->usrImage_len);


    DBG("Gev in image, flBootSize=%s, flKernelOffset=%s, flKernelSize=%s, flUsrImg1Offset=%s, flUsrImg1Size=%s,flUsrImg2Offset=%s, flUsrImg2Size=%s, flVer=%s\r\n",
        pImageInfo->flBootSize, pImageInfo->flKernelOffset, 
        pImageInfo->flKernelSize, pImageInfo->flUsrImg1Offset, pImageInfo->flUsrImg1Size, pImageInfo->flUsrImg2Offset, pImageInfo->flUsrImg2Size,pImageInfo->flVer);

    rc = OK;
RET:
    pImageInfo->isValid = ((rc == OK) ? TRUE : FALSE);
    DBG("Parsing image done\r\n");
    //free(pUndesBuf);
    return rc;    
}

#endif


