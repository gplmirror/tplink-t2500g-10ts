# See package/makedevs/README for details
#
# This device table is used only to create device files when a static
# device configuration is used (entries in /dev are static).
#
# <name>			<type>	<mode>	<uid>	<gid>	<major>	<minor>	<start>	<inc>	<count>

# Normal system devices
/dev/mem			c			644		0		0		1			1			0			0		-
/dev/kmem		c			644		0		0		1			2			0			0		-
/dev/null		c			644		0		0		1			3			0			0		-
/dev/zero		c			644		0		0		1			5			0			0		-
/dev/random		c			644		0		0		1			8			0			0		-
/dev/urandom	c			644		0		0		1			9			0			0		-
/dev/console	c			644		0		0		5			1			-			-		-
/dev/tty			c			644		0		0		5			0			-			-		-
/dev/tty			c			644		0		0		4			0			0			1		4
/dev/ttyp		c			644		0		0		3			0			0			1		10
/dev/ttypa		c			644		0		0		3			10			-			-		-
/dev/ttypb		c			644		0		0		3			11			-			-		-
/dev/ttypc		c			644		0		0		3			12			-			-		-
/dev/ttypd		c			644		0		0		3			13			-			-		-
/dev/ttype		c			644		0		0		3			14			-			-		-
/dev/ttypf		c			644		0		0		3			15			-			-		-
/dev/ptmx		c			644		0		0		5			2			-			-		-
/dev/ptyp		c			644		0		0		2			0			0			1		10
/dev/ptypa		c			644		0		0		2			10			-			-		-
/dev/ptypb		c			644		0		0		2			11			-			-		-
/dev/ptypc		c			644		0		0		2			12			-			-		-
/dev/ptypd		c			644		0		0		2			13			-			-		-
/dev/ptype		c			644		0		0		2			14			-			-		-
/dev/ptypf		c			644		0		0		2			15			-			-		-
/dev/ptmx		c			644		0		0		5			2			-			-		-
/dev/ttyS		c			644		0		0		4			64			0			1		2

# <name>			<type>	<mode>	<uid>	<gid>	<major>	<minor>	<start>	<inc>	<count>
# MTD stuff
/dev/mtdblock	b			644		0		0		31			0			0			1		12
/dev/mtdchar	c			644		0		0		90			0			0			1		12

/dev/cua			c			644		0		0		5			64			0			1		2

/dev/rtcore		c			644		0		0		200		0			-			-		-
/dev/ski			c			644		0		0		201		0			-			-		-

